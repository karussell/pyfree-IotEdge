#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _BUSINESS_DEF_H_
#define _BUSINESS_DEF_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_read.h
  *File Mark       : 
  *Summary         : 
  *业务数据集的前置声明,属于单体类,相当于业务数据相关的全局变量+全局函数集
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "conf_read.h"
#include "conf_app.h"
#include "appconf_data.h"

class BusinessDef : public AppConfData
{
public:
	static BusinessDef* getInstance();
	static void Destroy();
	~BusinessDef();
public:
private:
	BusinessDef();
	BusinessDef& operator=(const BusinessDef&) { return *this; };
	void init(); 
private:
	static BusinessDef* instance;
};

#endif


