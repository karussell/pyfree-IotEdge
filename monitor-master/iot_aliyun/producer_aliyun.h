#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PRODUCER_ALIYUN_H_
#define _PRODUCER_ALIYUN_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : producer_aliyun.h
  *File Mark       :
  *Summary         : 阿里云物联网的发布线程实现,暂时没采用
  *
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

class ProducerAliyun : public acl::thread
{
public:
	ProducerAliyun();
	virtual ~ProducerAliyun();
	//
	void* run();
private:
  //禁止拷贝构造和赋值运算
	ProducerAliyun(const ProducerAliyun&);
	ProducerAliyun& operator=(const ProducerAliyun&) { return *this; };
private:
	bool running;   //线程运行标记

};

#endif 
