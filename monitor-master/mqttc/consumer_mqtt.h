#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _COMSUMER_MQTT_H_
#define _COMSUMER_MQTT_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : consumer_mqtt.h
  *File Mark       :
  *Summary         : mqtt通信的消费类接口实现
  *
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "mqttFunc.h"

class PyMQTTIO;

class ConsumerMQTT : public acl::thread
{
public:
	ConsumerMQTT(PyMQTTIO *mqttcpp_);
	virtual ~ConsumerMQTT();

	void* run();
private:
  /**
	 * 从缓存队列中获取数据并进行帧解析\校验等业务处理
	 * @return {void}
	 */
	void frameProcess();
private:
  //禁止拷贝构造和赋值运算
	ConsumerMQTT(const ConsumerMQTT&);
	ConsumerMQTT& operator=(const ConsumerMQTT&) { return *this; };
private:
	bool running;       //线程运行标记
	PyMQTTIO *mqttcpp;  //mqtt通信接口
	MQTTFUNC mqttfunc;  //mqtt业务逻辑处理
};


#endif
