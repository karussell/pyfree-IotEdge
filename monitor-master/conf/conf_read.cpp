#include "conf_read.h"

#include <map>
#include <fstream>
#include <time.h>

#include "pfunc.h"
#include "Log.h"
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

void read_LocalLinkConf(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::LocalLinkConf &lconf)
{
	while (pchild){
		if (0==strcmp("GatherIp" , pchild->tag_name()))
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				lconf.gather_ip = std::string(val_);
		}
		if (0==strcmp("GatherPort" , pchild->tag_name()))
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				lconf.gather_port = atoi(val_);
		}
		//
		if (0==strcmp("TranFunc",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				lconf.tranFunc = atoi(val_) > 0 ? true : false;
		}
		//
		if (0==strcmp("TranIP" , pchild->tag_name())) 
		{		
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				lconf.ipclient = std::string(val_);
		}
		//
		if (0==strcmp("TranPort" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				lconf.portClient = atoi(val_);
		}
		//
		if (0==strcmp("LocalSocketFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				lconf.localSocketFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("LocalSocketIp" , pchild->tag_name()))
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				lconf.ipLocalSocket = std::string(val_);
		}
		if (0==strcmp("LocalSocketPort" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				lconf.portLocalSocket = atoi(val_);
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nLocalLinkConf:\nGatherIp:%s\nGatherPort:%d\nTranFunc:%d\nTranIP:%s\nTranPort:%d"
	// 	"\nLocalSocketFunc:%d\nLocalSocketIp:%s\nLocalSocketPort:%d\r\n"
	// 	,lconf.gather_ip.c_str()
	// 	,lconf.gather_port
	// 	,lconf.tranFunc
	// 	,lconf.ipclient.c_str()
	// 	,lconf.portClient
	// 	,lconf.localSocketFunc
	// 	,lconf.ipLocalSocket.c_str()
	// 	,lconf.portLocalSocket);
}

void read_SysAssistConf(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::SysAssistConf &saconf)
{
	while (pchild){
		if (0==strcmp("DiskSymbol",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (1 == strlen(val_))
				saconf.diskSymbol = val_[0];
		}
		//
		if (0==strcmp("FreeSizeLimit",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.freeSizeLimit = atoi(val_);
			if (saconf.freeSizeLimit < 1000)
				saconf.freeSizeLimit = 1000;
		}
		//
		if (0==strcmp("DayForLimit",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.dayForLimit = atoi(val_);
		}
		if (0==strcmp("LogDir",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.gLogDir = std::string(val_);
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nSysAssistConf:\nDiskSymbol:%c\nFreeSizeLimit:%d\nDayForLimit:%d\nLogDir:%s\r\n"
	// 	,saconf.diskSymbol
	// 	,saconf.freeSizeLimit
	// 	,saconf.dayForLimit
	// 	,saconf.gLogDir.c_str());
}

void read_RecordConf(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::RecordConf &recconf)
{
	while (pchild){
		//
		if (0==strcmp("RecordFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				recconf.recordFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("PeerIp",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				recconf.peer_ip = std::string(val_);
		}
		if (0==strcmp("PeerPort",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				recconf.peer_port = atoi(val_);
		}
		if (0==strcmp("LocalIp",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				recconf.local_ip = std::string(val_);
		}
		if (0==strcmp("LocalPort",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				recconf.local_port = atoi(val_);
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nRecordConf:\nRecordFunc:%d\nPeerIp:%s\nPeerPort:%d\nLocalIp:%s\nLocalPort:%d\r\n"
	// 	,recconf.recordFunc
	// 	,recconf.peer_ip.c_str()
	// 	,recconf.peer_port
	// 	,recconf.local_ip.c_str()
	// 	,recconf.local_port);
}

void read_WPushConf(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::WaringPushConf &wpconf)
{
	while (pchild){
		//
		if (0==strcmp("wpFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				wpconf.wpFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("PeerIp",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				wpconf.peer_ip = std::string(val_);
		}
		if (0==strcmp("PeerPort",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				wpconf.peer_port = atoi(val_);
		}
		if (0==strcmp("LocalIp",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				wpconf.local_ip = std::string(val_);
		}
		if (0==strcmp("LocalPort",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				wpconf.local_port = atoi(val_);
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nWPushConf:\nwpFunc:%d\nPeerIp:%s\nPeerPort:%d\nLocalIp:%s\nLocalPort:%d\r\n"
	// 	,wpconf.wpFunc
	// 	,wpconf.peer_ip.c_str()
	// 	,wpconf.peer_port
	// 	,wpconf.local_ip.c_str()
	// 	,wpconf.local_port);
}

void read_BusinessConf(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::BusinessConf &bsconf)
{
	while (pchild){
		if (0==strcmp("DevPath",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				bsconf.devPath = std::string(val_);
		}
		if (0==strcmp("PmapPath",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				bsconf.pmapPath = std::string(val_);
		}
		if (0==strcmp("PlanFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				bsconf.planFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("PlanPath",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				bsconf.planPath = std::string(val_);
		}
		if (0==strcmp("AlarmFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				bsconf.alarmFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("AlarmPath",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				bsconf.alarmPath = std::string(val_);
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nBusinessConf:\nDevPath:%s\nPmapPath:%s\nPlanFunc:%d\nPlanPath:%s\nAlarmFunc:%d\nAlarmPath:%s\r\n"
	// 	,bsconf.devPath.c_str()
	// 	,bsconf.pmapPath.c_str()
	// 	,bsconf.planFunc
	// 	,bsconf.planPath.c_str()
	// 	,bsconf.alarmFunc
	// 	,bsconf.alarmPath.c_str());
}

void read_AreaInfo(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::AreaInfo &ainfo)
{
	while (pchild){
		if (0==strcmp("AreaID",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				ainfo.areaId = atoi(val_);
		}
		if (0==strcmp("AreaType",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				ainfo.areaType = atoi(val_);
		}
		if (0==strcmp("AreaName",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				ainfo.areaName = std::string(val_);
		}
		if (0==strcmp("AreaDesc",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				ainfo.areaDesc = std::string(val_);
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nAreaInfo:\nAreaID:%d\nAreaType:%d\nAreaName:%s\nAreaDesc:%s\r\n"
	// 	,ainfo.areaId
	// 	,ainfo.areaType
	// 	,ainfo.areaName.c_str()
	// 	,ainfo.areaDesc.c_str());
}

void read_UpdateConf(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::UpdateConf &uconf)
{
	while (pchild){
		if (0==strcmp("TimeUpFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				uconf.timeUpFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("TimeUpInterval",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				uconf.timeUpInterval = atoi(val_);
			if (uconf.timeUpInterval < 10) 
			{
				uconf.timeUpInterval = 10;
			}
		}
		if (0==strcmp("WaringEventFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				uconf.waringEventFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("VerificationFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				uconf.verificationFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("TimeCheckInterval",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				uconf.timeCheckInterval = atoi(val_);
			if (uconf.timeCheckInterval < 30) 
			{
				uconf.timeCheckInterval = 30;
			}
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nUpdateConf:\nTimeUpFunc:%d\nTimeUpInterval:%d\nWaringEventFunc:%d\nVerificationFunc:%d\nTimeCheckInterval:%d\r\n"
	// 	,uconf.timeUpFunc
	// 	,uconf.timeUpInterval
	// 	,uconf.waringEventFunc
	// 	,uconf.verificationFunc
	// 	,uconf.timeCheckInterval);
}

void read_HttpWeather(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::HttpWeather &hweather)
{
	while (pchild){
		///////////////////
		if (0==strcmp("HttpWeatherFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				hweather.httpWeatherFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("WeatherStart",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				hweather.startHourWeather = atoi(val_);
		}
		if (0==strcmp("WeatherEnd",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				hweather.endHourWeather = atoi(val_);
		}
		if (0==strcmp("WeatherThreeHourCity" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				hweather.threeHourCityWeather = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("WeatherHost",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				hweather.weatherhost = std::string(val_);
		}
		if (0==strcmp("WeatherPath",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				hweather.weatherpath = std::string(val_);
		}
		if (0==strcmp("WeatherPort",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				hweather.weatherport = atoi(val_);
		}
		if (0==strcmp("WeatherCity",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				hweather.weathercity = std::string(val_);
		}
		if (0==strcmp("WeatherAuthorization",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				hweather.weatherauthorization = std::string(val_);
		}
		if (0==strcmp("WeatherUptime",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				hweather.weatherUpTime = atoi(val_);
			if (hweather.weatherUpTime < 5) 
			{
				hweather.weatherUpTime = 5;
			}
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nHttpWeather:\nHttpWeatherFunc:%d\nWeatherStart:%d\nWeatherEnd:%d\nWeatherThreeHourCity:%d\n"
	// 	"WeatherHost:%s\nWeatherPath:%s\nWeatherPort:%d\nWeatherCity:%s\nWeatherAuthorization:%s\nWeatherUptime:%d\r\n"
	// 	,hweather.httpWeatherFunc
	// 	,hweather.startHourWeather
	// 	,hweather.endHourWeather
	// 	,hweather.threeHourCityWeather
	// 	,hweather.weatherhost.c_str()
	// 	,hweather.weatherpath.c_str()
	// 	,hweather.weatherport
	// 	,hweather.weathercity.c_str()
	// 	,hweather.weatherauthorization.c_str()
	// 	,hweather.weatherUpTime);
}

void read_AliyunConf(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::AliyunConf &aliconf)
{
	while (pchild){
		//////
		if (0==strcmp("AliyunIoFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				aliconf.aliyunIoFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("product_key",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				aliconf.gateway.product_key = std::string(val_);
		}
		if (0==strcmp("product_secret",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				aliconf.gateway.product_secret = std::string(val_);
		}
		if (0==strcmp("device_name",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				aliconf.gateway.device_name = std::string(val_);
		}
		if (0==strcmp("device_secret",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				aliconf.gateway.device_secret = std::string(val_);
		}
		if (0==strcmp("ali_event_flag",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				aliconf.aliEI.aliEventFlag = std::string(val_);
		}
		if (0==strcmp("ali_event_out",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				aliconf.aliEI.aliEventOut = std::string(val_);
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nAliyunConf:\nAliyunIoFunc:%d\nproduct_key:%s\nproduct_secret:%s\ndevice_name:%s\ndevice_secret:%s\n"
	// 	"ali_event_flag:%s\nali_event_out:%s\r\n"
	// 	,aliconf.aliyunIoFunc
	// 	,aliconf.gateway.product_key.c_str()
	// 	,aliconf.gateway.product_secret.c_str()
	// 	,aliconf.gateway.device_name.c_str()
	// 	,aliconf.gateway.device_secret.c_str()
	// 	,aliconf.aliEI.aliEventFlag.c_str()
	// 	,aliconf.aliEI.aliEventOut.c_str());
}

void read_MqttConf(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::MqttConf &mqttconf)
{
	while (pchild){
		if (0==strcmp("MqttFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				mqttconf.mqttFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("MqttIp",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				mqttconf.ip = std::string(val_);
		}
		if (0==strcmp("MqttPort" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				mqttconf.port= atoi(val_);
		}
		if (0==strcmp("MqttUser",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				mqttconf.user = std::string(val_);
		}
		if (0==strcmp("MqttPassword",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				mqttconf.password = std::string(val_);
		}
		if (0==strcmp("MqttJson" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				mqttconf.json_flag = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("MqttCheck" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				mqttconf.hmac_sha_func = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("MqttKey",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				mqttconf.hmac_sha_key = std::string(val_);
		}
		pchild = pnode->next_child();
	}
}

//
void pyfree::readAppConf(AppConf &conf, std::string xml_ )
{
	try
	{
        acl::string buf;
		if (acl::ifstream::load(xml_.c_str(), &buf) == false)
		{
			Print_WARN("load %s error %s\r\n", xml_.c_str(), acl::last_serror());
			return;
		}

		acl::xml1 xml;
		xml.update(buf);

		acl::xml_node* node 	= &(xml.get_root());
		acl::xml_node* child 	= node->first_child();
        while (child)
		{
			if(0==strcmp(child->tag_name(), "appconf"))
			{
				node	= child;
				child 	= node->first_child();
				while (child)
				{
					//
					if (0==strcmp("LocalLinkConf",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_LocalLinkConf(pnode,pchild,conf.lconf);
					}
					if (0==strcmp("SysAssistConf",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_SysAssistConf(pnode,pchild,conf.saconf);
					}
					if (0==strcmp("RecordConf",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_RecordConf(pnode,pchild,conf.recconf);
					}
					if (0==strcmp("WPConf",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_WPushConf(pnode,pchild,conf.wpconf);
					}
                    if (0==strcmp("BusinessConf",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_BusinessConf(pnode,pchild,conf.bsconf);
					}
					if (0==strcmp("AreaInfo",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_AreaInfo(pnode,pchild,conf.ainfo);
					}
					if (0==strcmp("UpdateConf",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_UpdateConf(pnode,pchild,conf.uconf);
					}
					if (0==strcmp("HttpWeather",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_HttpWeather(pnode,pchild,conf.hweather);
					}
					if (0==strcmp("AliyunConf",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_AliyunConf(pnode,pchild,conf.aliconf);
					}
					if (0==strcmp("MqttConf",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_MqttConf(pnode,pchild,conf.mqttconf);
					}
					child = node->next_child();
				}
				break;
			}
			child = node->next_child();
		}
		CLogger::createInstance()->Log(MsgInfo,"success for read %s!\n",xml_.c_str());
	}
	catch (...)
	{
		CLogger::createInstance()->Log(MsgError,
			"read xml file[%s] error: %s %s %d,please check the file format and code!\n"
			, xml_.c_str(), __FILE__, __FUNCTION__, __LINE__);
	}
};

void pyfree::writeAppConf(const AppConf conf, std::string xml_)
{
	try {
		
	}
	catch (...)
	{
		CLogger::createInstance()->Log(MsgError
			, "write xml file[%s] error[%s]: %s %s %d"
			", please check the code!"
			, xml_.c_str()
			,__FILE__, __FUNCTION__, __LINE__);
	}
};

//get node attr
static void getAtt(const acl::xml_node* node,std::map<std::string,std::string> &atts_)
{
	const acl::xml_attr* attr = node->first_attr();
	while (attr)
	{
		std::map<std::string,std::string>::iterator it = atts_.find(std::string(attr->get_name()));
		if(it != atts_.end())
		{
			it->second = std::string(attr->get_value());
		}
		attr = node->next_attr();
	}
};

void init_dev_atts(std::map<std::string, std::string> &atts_)
{
	atts_["devID"] = "";
	atts_["devType"] = "";
	atts_["gID"] = "";
	atts_["name"] = "";
	atts_["desc"] = "";
	atts_["product_key"] = "";
	atts_["product_secret"] = "";
	atts_["device_name"] = "";
	atts_["device_secret"] = "";
};

void read_dev_atts(pyfree::Dev &dev_, std::map<std::string, std::string> atts_)
{
	//def property
	dev_.devInfo.devID = atoll(atts_["devID"].c_str());
	dev_.devInfo.devType = (pyfree::DevType)atoi(atts_["devType"].c_str());
	dev_.devInfo.gID = atoll(atts_["gID"].c_str());
	dev_.devInfo.name = atts_["name"];
	dev_.devInfo.desc = atts_["desc"];
	//设备描述服务内部没使用,推送客户端和云端采用了UTF-8
//#ifdef WIN32
	// dev_.devInfo.desc = UTF_82ASCII(dev_.devInfo.desc);
//#endif
	//
	dev_.devInfo.device_triples.product_key = atts_["product_key"];
	dev_.devInfo.device_triples.product_secret = atts_["product_secret"];
	dev_.devInfo.device_triples.device_name = atts_["device_name"];
	dev_.devInfo.device_triples.device_secret = atts_["device_secret"];
	//extend property
	for (std::map<std::string, std::string>::iterator it = atts_.begin(); it!=atts_.end();++it)
	{
		if (pyfree::containStr_start(it->first,"tag_"))
		{
			std::string::size_type pos_f = it->first.find("_");//
			if (pos_f != std::string::npos)
			{
				std::string key_str = it->first.substr(pos_f + 1, it->first.length()-4);
				dev_.devInfo.dev_tags[key_str] = it->second;
			}
		}
	}
	// Print_NOTICE("\ndev_atts:\ndevID:%lld\ndevType:%d\ngID:%lld\nname:%s\ndesc:%s\n"
	// 	"product_key:%s\nproduct_secret:%s\ndevice_name:%s\ndevice_secret:%s\n"
	// 	,dev_.devInfo.devID
	// 	,dev_.devInfo.devType
	// 	,dev_.devInfo.gID
	// 	,dev_.devInfo.name.c_str()
	// 	,dev_.devInfo.desc.c_str()
	// 	,dev_.devInfo.device_triples.product_key.c_str()
	// 	,dev_.devInfo.device_triples.product_secret.c_str()
	// 	,dev_.devInfo.device_triples.device_name.c_str()
	// 	,dev_.devInfo.device_triples.device_secret.c_str());
	// for(std::map<std::string, std::string>::iterator it = dev_.devInfo.dev_tags.begin(); it!=dev_.devInfo.dev_tags.end();++it)
	// {
	// 	printf("%s:%s\n",it->first.c_str(),it->second.c_str());
	// }
};

void init_PInfo_atts(std::map<std::string, std::string> &atts_)
{
	atts_["pID"] = "";
	atts_["pType"] = "";
	atts_["name"] = "";
	atts_["desc"] = "";
	atts_["defaultVal"] = "";
	atts_["aliyun_key"] = "";
	//
	atts_["ratio"] = "";
	atts_["base"] = "";
	//
	atts_["changeRecord"] = "";
	atts_["changeUp"] = "";
	//
	atts_["clientOrder"] = "";
	atts_["UpLoopTime"] = "";
	atts_["UpLoopTime_ali"] = "";
	//
	atts_["ValLimitUp"] = "";
	atts_["ValLimitDown"] = "";
	atts_["ComValLimit"] = "";
	atts_["ComRate"] = "";
	atts_["shakeInterval"] = "";
	//
	atts_["EvtStartTime"] = "";
	atts_["EvtEndTime"] = "";
	atts_["NCHTInterval"] = "";
	atts_["EvtNoChangeLimit"] = "";
	atts_["NUPTInterval"] = "";
	atts_["EvtNoUpdateLimit"] = "";
	atts_["EWay"] = "";
};

void read_PInfo_atts(pyfree::PInfo &pinfo_,std::map<std::string, std::string> atts_)
{
	pinfo_.ratio = 1.0;
	pinfo_.base = 0.0;
	pinfo_.ValChange = false;
	pinfo_.changeRecord = true;
	//pinfo_.changeUp = true;
	pinfo_.markT = 0;
	pinfo_.markT_ali = 0;
	pinfo_.pID = atoi(atts_["pID"].c_str());
	pinfo_.pType = (pyfree::PType)atoi(atts_["pType"].c_str());
	pinfo_.name = atts_["name"];
	pinfo_.desc = atts_["desc"];
	//信息点描述服务内部没使用,推送客户端和云端采用了UTF-8
//#ifdef WIN32
	// pinfo_.desc = UTF_82ASCII(pinfo_.desc);
//#endif
	pinfo_.aliyun_key = "";
	if (!atts_["aliyun_key"].empty()) 
	{
		pinfo_.aliyun_key = atts_["aliyun_key"];
	}
	pinfo_.defVal = 0.0;
	pinfo_.valTimeClock = 0;
	pinfo_.valUpdateClock = 0;
	if (!atts_["defaultVal"].empty()) 
	{
		pinfo_.defVal = static_cast<float>(atof(atts_["defaultVal"].c_str()));
	}
	if (!atts_["ratio"].empty())
	{
		pinfo_.ratio = static_cast<float>(atof(atts_["ratio"].c_str()));
	}
	if (!atts_["base"].empty())
	{
		pinfo_.base = static_cast<float>(atof(atts_["base"].c_str()));
	}
	//Print_NOTICE("devID(%lld),pID(%d),ratio(%.3f),base(%.3f)\n"
	//	, _dev.devInfo.devID,pinfo_.pID,pinfo_.ratio, pinfo_.base);
	if (!atts_["changeRecord"].empty())
	{
		pinfo_.changeRecord = atoi(atts_["changeRecord"].c_str()) > 0 ? true : false;
	}
	//if (!atts_["changeUp"].empty())
	//{
	//	pinfo_.changeUp = atoi(atts_["changeUp"].c_str()) > 0 ? true : false;
	//}
	pinfo_.clientOrder = 1;
	if (!atts_["clientOrder"].empty())
	{
		pinfo_.clientOrder = atoi(atts_["clientOrder"].c_str());
	}
	pinfo_.upLoopTime = 10;
	if (!atts_["UpLoopTime"].empty())
	{
		pinfo_.upLoopTime = atoi(atts_["UpLoopTime"].c_str());
		if (pinfo_.upLoopTime < 3)
		{
			pinfo_.upLoopTime = 3;
		}
	}
	pinfo_.upLoopTime_ali = 60;
	if (!atts_["UpLoopTime_ali"].empty())
	{
		pinfo_.upLoopTime_ali = atoi(atts_["UpLoopTime_ali"].c_str());
		if (pinfo_.upLoopTime_ali < 60)
		{
			pinfo_.upLoopTime_ali = 60;
		}
	}
	pinfo_.valLimitUp = 9999.0;
	if (!atts_["ValLimitUp"].empty()) 
	{
		pinfo_.valLimitUp = static_cast<float>(atof(atts_["ValLimitUp"].c_str()));
	}
	pinfo_.valLimitDown = -9999.0;
	if (!atts_["ValLimitDown"].empty()) 
	{
		pinfo_.valLimitDown = static_cast<float>(atof(atts_["ValLimitDown"].c_str()));
	}
	pinfo_.comValLimit = 100.0;
	if (!atts_["ComValLimit"].empty()) 
	{
		pinfo_.comValLimit = static_cast<float>(atof(atts_["ComValLimit"].c_str()));
	}
	pinfo_.comRate = 50.0;
	if (!atts_["ComRate"].empty()) 
	{
		pinfo_.comRate = static_cast<float>(atof(atts_["ComRate"].c_str()));
		if (pinfo_.comRate <= 0)
			pinfo_.comRate = 1;
	}
	pinfo_.shakeTimeInterval = 1000;
	if (!atts_["shakeInterval"].empty()) 
	{
		pinfo_.shakeTimeInterval = static_cast<unsigned int>(atoi(atts_["shakeInterval"].c_str()));
	}
	//
	pinfo_.evtStartTime = 0;
	if (!atts_["EvtStartTime"].empty()) 
	{
		pinfo_.evtStartTime = atoi(atts_["EvtStartTime"].c_str());
		if (pinfo_.evtStartTime < 0)
		{
			pinfo_.evtStartTime = 0;
		}
		if (pinfo_.evtStartTime > 1440)
		{
			pinfo_.evtStartTime = 1440;
		}
	}
	pinfo_.evtEndTime = 0;
	if (!atts_["EvtEndTime"].empty()) 
	{
		pinfo_.evtEndTime = atoi(atts_["EvtEndTime"].c_str());
		if (pinfo_.evtEndTime < 0)
		{
			pinfo_.evtEndTime = 0;
		}
		if (pinfo_.evtEndTime > 1440)
		{
			pinfo_.evtEndTime = 1440;
		}
	}
	pinfo_.evtForDay = 0;
	//不变化
	pinfo_.nChTInterval = 0;
	if (!atts_["NCHTInterval"].empty()) 
	{
		pinfo_.nChTInterval = atoi(atts_["NCHTInterval"].c_str());
		if (pinfo_.nChTInterval<100) 
		{
			pinfo_.nChTInterval = 100;
		}
	}
	pinfo_.evtNoChangeSendCountForDay = 0;
	pinfo_.evtNoChangeSendCountForLimit = 2;
	if (!atts_["EvtNoChangeLimit"].empty()) 
	{
		pinfo_.evtNoChangeSendCountForLimit = atoi(atts_["EvtNoChangeLimit"].c_str());
		if (pinfo_.evtNoChangeSendCountForLimit < 1)
		{
			pinfo_.evtNoChangeSendCountForLimit = 1;
		}
		if (pinfo_.evtNoChangeSendCountForLimit > 10)
		{
			pinfo_.evtNoChangeSendCountForLimit = 10;
		}
	}
	//不刷新
	pinfo_.nUpTInterval = 0;
	if (!atts_["NUPTInterval"].empty()) 
	{
		pinfo_.nUpTInterval = atoi(atts_["NUPTInterval"].c_str());
		if (pinfo_.nUpTInterval<100) 
		{
			pinfo_.nUpTInterval = 100;
		}
	}
	pinfo_.evtNoUpdateSendCountForDay = 0;
	pinfo_.evtNoUpdateSendCountForLimit = 2;
	if (!atts_["EvtNoUpdateLimit"].empty()) 
	{
		pinfo_.evtNoUpdateSendCountForLimit = atoi(atts_["EvtNoUpdateLimit"].c_str());
		if (pinfo_.evtNoUpdateSendCountForLimit < 1)
		{
			pinfo_.evtNoUpdateSendCountForLimit = 1;
		}
		if (pinfo_.evtNoUpdateSendCountForLimit > 10)
		{
			pinfo_.evtNoUpdateSendCountForLimit = 10;
		}
	}
	pinfo_.eway = pyfree::AlarmForLog;
	if (!atts_["EWay"].empty())
	{
		int eway_ = atoi(atts_["EWay"].c_str());
		switch (eway_) 
		{
		case 0:
			pinfo_.eway = pyfree::AlarmForDef;
			break;
		case 1:
			pinfo_.eway = pyfree::AlarmForLog;
			break;
		case 2:
			pinfo_.eway = pyfree::AlarmForSMS;
			break;
		case 3:
			pinfo_.eway = pyfree::AlarmForEMail;
			break;
		default:
			break;
		}
	}
	// Print_NOTICE("\nPInfo_atts:\npID:%d\npType:%d\nname:%s\ndesc:%s\norgType:%d\naliyun_key:%s\n"
	// 	"defaultVal:%f\nratio:%f\nbase:%f\nchangeRecord:%d\nclientOrder:%d\nclientType:%d\n"
	// 	"UpLoopTime:%d\nUpLoopTime_ali:%d\nValLimitUp:%f\nValLimitDown:%f\nComValLimit:%f\n"
	// 	"ComRate:%f\nshakeInterval:%d\nEvtStartTime:%d\nEvtEndTime:%d\nNCHTInterval:%d\n"
	// 	"EvtNoUpdateLimit:%d\nEWay:%d\r\n"
	// 	,pinfo_.pID
	// 	,pinfo_.pType
	// 	,pinfo_.name.c_str()
	// 	,pinfo_.desc.c_str()
	// 	,pinfo_.orgType
	// 	,pinfo_.aliyun_key.c_str()
	// 	,pinfo_.defVal
	// 	,pinfo_.ratio
	// 	,pinfo_.base
	// 	,pinfo_.changeRecord
	// 	,pinfo_.clientOrder
	// 	,pinfo_.clientType
	// 	,pinfo_.upLoopTime
	// 	,pinfo_.upLoopTime_ali
	// 	,pinfo_.valLimitUp
	// 	,pinfo_.valLimitDown
	// 	,pinfo_.comValLimit
	// 	,pinfo_.comRate
	// 	,pinfo_.shakeTimeInterval
	// 	,pinfo_.evtStartTime
	// 	,pinfo_.evtEndTime
	// 	,pinfo_.nChTInterval
	// 	,pinfo_.evtNoUpdateSendCountForLimit
	// 	,pinfo_.eway);
};

void pyfree::readDevs(std::map<unsigned long long,Dev> &devs, std::string xml_)
{
	try
	{
        acl::string buf;
		if (acl::ifstream::load(xml_.c_str(), &buf) == false)
		{
			Print_WARN("load %s error %s\r\n", xml_.c_str(), acl::last_serror());
			return;
		}
		Print_NOTICE("load %s success\r\n", xml_.c_str());
		acl::xml1 xml;
		xml.update(buf);

		acl::xml_node* node 	= &(xml.get_root());
		acl::xml_node* child 	= node->first_child();
        while (child)
		{
			// printf("tag_name[1]: %s\n", child->tag_name());
			if(0==strcmp(child->tag_name(), "devs"))
			{
				acl::xml_node* dnode	= child;
				acl::xml_node* dchild 	= dnode->first_child();
				while (dchild)
				{
					// printf("tag_name[2]: %s\n", dchild->tag_name());
					//
					if (0==strcmp("dev",dchild->tag_name())) 
					{
						std::map<std::string,std::string> atts_;
						init_dev_atts(atts_);
						getAtt(dchild,atts_);
						pyfree::Dev dev_;
						read_dev_atts(dev_,atts_);
						//
						acl::xml_node* pnode	= dchild;
						acl::xml_node* pchild 	= pnode->first_child();
						while (pchild)
						{
							if (0==strcmp("pInfo",pchild->tag_name())) 
							{
								atts_.clear();
								init_PInfo_atts(atts_);
								getAtt(pchild,atts_);
								PInfo pinfo_;
								read_PInfo_atts(pinfo_,atts_);
								dev_.pInfo[pinfo_.pID] = pinfo_;
							}
							pchild 	= pnode->next_child();
						}
						devs[dev_.devInfo.devID] = dev_;
					}
					dchild 	= dnode->next_child();
				}
			}
			child = node->next_child();
		}
		CLogger::createInstance()->Log(MsgInfo,"success for read %s!\n",xml_.c_str());
	}
	catch (...)
	{
		CLogger::createInstance()->Log(MsgError,
			"read xml file[%s] error: %s %s %d,please check the file format and code!"
			, xml_.c_str(),__FILE__, __FUNCTION__, __LINE__);
	}
};

void init_from_atts(std::map<std::string, std::string> &atts_)
{
	atts_["ip"] = "";
	atts_["ID"] = "";
	atts_["pType"] = "";
};

void read_from_atts(pyfree::PMap &pmap_,std::map<std::string, std::string> atts_)
{
	pmap_.from.ipStr = atts_["ip"];
	pmap_.from.pID = atoi(atts_["ID"].c_str());
	pmap_.from.pType = (pyfree::PType)atoi(atts_["pType"].c_str());
	pmap_.from.ipLong = pyfree::ipToInt(pmap_.from.ipStr);
	// Print_NOTICE("\nfrom_atts:\nip:%s\npid:%d\nptype:%d\niplong:%llu\r\n"
	// 	,pmap_.from.ipStr.c_str()
	// 	,pmap_.from.pID
	// 	,pmap_.from.pType
	// 	,pmap_.from.ipLong);
};

void init_to_atts(std::map<std::string, std::string> &atts_)
{
	atts_["devID"] = "";
	atts_["devType"] = "";
	atts_["pID"] = "";
	atts_["pType"] = "";
};

void read_to_atts(pyfree::PMap &pmap_, std::map<std::string, std::string> atts_)
{
	pmap_.to.devID = atoll(atts_["devID"].c_str());
	pmap_.to.devType = (pyfree::DevType)atoi(atts_["devType"].c_str());
	pmap_.to.pID = atoi(atts_["pID"].c_str());
	pmap_.to.pType = (pyfree::PType)atoi(atts_["pType"].c_str());
	// Print_NOTICE("\nto_atts:\ndevid:%lld\ndevtype:%d\npid:%u\nptype:%d\r\n"
	// 	,pmap_.to.devID
	// 	,pmap_.to.devType
	// 	,pmap_.to.pID
	// 	,pmap_.to.pType);
};

void pyfree::readPMaps(std::vector<PMap> &pmaps, std::string xml_)
{
	try
	{
		acl::string buf;
		if (acl::ifstream::load(xml_.c_str(), &buf) == false)
		{
			Print_WARN("load %s error %s\r\n", xml_.c_str(), acl::last_serror());
			return;
		}

		acl::xml1 xml;
		xml.update(buf);

		acl::xml_node* node 	= &(xml.get_root());
		acl::xml_node* child 	= node->first_child();
        while (child)
		{
			if(0==strcmp(child->tag_name(), "maps"))
			{
				acl::xml_node* pnode	= child;
				acl::xml_node* pchild 	= pnode->first_child();
				while (pchild)
				{
					if(0==strcmp(pchild->tag_name(), "map"))
					{
						acl::xml_node* fnode	= pchild;
						acl::xml_node* fchild 	= fnode->first_child();
						PMap pmap_;
						while (fchild)
						{
							//
							if(0==strcmp(fchild->tag_name(), "from"))
							{
								std::map<std::string, std::string> atts_;
								init_from_atts(atts_);
								getAtt(fchild,atts_);
								read_from_atts(pmap_,atts_);
							}
							if(0==strcmp(fchild->tag_name(), "to"))
							{
								std::map<std::string, std::string> atts_;
								init_to_atts(atts_);
								getAtt(fchild,atts_);
								read_to_atts(pmap_,atts_);
							}
							fchild = fnode->next_child();
						}
						pmaps.push_back(pmap_);
					}
					pchild = pnode->next_child();
				}
			}
			child = node->next_child();
		}
		CLogger::createInstance()->Log(MsgInfo,"success for read %s!\n",xml_.c_str());
	}
	catch (...)
	{
		CLogger::createInstance()->Log(MsgError
			, "read xml file[%s] error: %s %s %d"
			", please check the file format and code!"
			, xml_.c_str()
			,__FILE__, __FUNCTION__, __LINE__);
	}
};

void init_plan_atts(std::map<std::string, std::string> &atts_)
{
	atts_["planID"] = "";
	atts_["planType"] = "";
	atts_["desc"] = "";
	atts_["planEffect"] = "";
	atts_["planInterval"] = "";
};

void read_plan_atts(pyfree::PlanProperty &planAtt,std::map<std::string, std::string> atts_)
{
	//
	planAtt.planID = atoi(atts_["planID"].c_str());
	planAtt.planType = (pyfree::PlanType)atoi(atts_["planType"].c_str());
	planAtt.desc = atts_["desc"];
//#ifdef WIN32
	// planAtt.desc = UTF_82ASCII(planAtt.desc);//当前是监控服务独占的应用需求
//#endif
	planAtt.planEffect = 0;
	if (!atts_["planEffect"].empty())
	{
		planAtt.planEffect = atoi(atts_["planEffect"].c_str());
		if (planAtt.planEffect<0)
		{
			planAtt.planEffect = 0;
		}
	}
	planAtt.planSuccessFlag = 0;
	planAtt.planInterval = 0;
	if (!atts_["planInterval"].empty())
	{
		planAtt.planInterval = atoi(atts_["planInterval"].c_str());
	}
	// Print_NOTICE("\nplan_atts:\nplanID:%d\nplanType:%d\ndesc:%s\nplanEffect:%d\nplanInterval:%lu\r\n"
	// 	,planAtt.planID
	// 	,planAtt.planType
	// 	,planAtt.desc.c_str()
	// 	,planAtt.planEffect
	// 	,planAtt.planInterval);
};

void init_dt_atts(std::map<std::string, std::string> &atts_)
{
	atts_["year"] = "";
	atts_["month"] = "";
	atts_["day"] = "";
	atts_["hour"] = "";
	atts_["min"] = "";
	atts_["wday"] = "";
};

void read_dt_atts(pyfree::PlanTime &ptime, std::map<std::string, std::string> atts_)
{
	ptime.myear = ("*" == atts_["year"] ? -1 : atoi(atts_["year"].c_str()));
	ptime.mmon = ("*" == atts_["month"] ? -1 : atoi(atts_["month"].c_str()));
	ptime.mday = ("*" == atts_["day"] ? -1 : atoi(atts_["day"].c_str()));
	ptime.mhour = ("*" == atts_["hour"] ? -1 : atoi(atts_["hour"].c_str()));
	ptime.mmin = ("*" == atts_["min"] ? -1 : atoi(atts_["min"].c_str()));
	if (!atts_["wday"].empty()) 
	{
		ptime.wday = ("*" == atts_["wday"] ? -1 : atoi(atts_["wday"].c_str()));
	}
	else 
	{
		ptime.wday = -1;
	}

	ptime.flag = false;
	// Print_NOTICE("\ndt_atts:\nyear:%d\nmonth:%d\nday:%d\nhour:%d\nmin:%d\nwday:%d\r\n"
	// 	,ptime.myear
	// 	,ptime.mmon
	// 	,ptime.mday
	// 	,ptime.mhour
	// 	,ptime.mmin
	// 	,ptime.wday);
	//校验 年 月 日 时 分 星期是否越限
	//your code
};

void init_et_atts(std::map<std::string, std::string> &atts_)
{
	atts_["year"] = "";
	atts_["month"] = "";
	atts_["day"] = "";
};

void read_et_atts(pyfree::ExceptTime &et_, std::map<std::string, std::string> atts_)
{
	et_.myear = ("*" == atts_["year"] ? -1 : atoi(atts_["year"].c_str()));
	et_.mmon = ("*" == atts_["month"] ? -1 : atoi(atts_["month"].c_str()));
	et_.mday = ("*" == atts_["day"] ? -1 : atoi(atts_["day"].c_str()));
	// Print_NOTICE("\net_atts:\nyear:%d\nmonth:%d\nday:%d\r\n"
	// 	,et_.myear
	// 	,et_.mmon
	// 	,et_.mday);
};

//filter old time plan
bool checkFutureTimePlan(pyfree::PlanTime ptime)
{
	time_t _t = time(NULL);
	struct tm _tt;
#ifdef WIN32
	localtime_s(&_tt, &_t);//系统本地时间
#else
	localtime_r(&_t, &_tt);//
#endif
	_tt.tm_year += 1900;
	_tt.tm_mon += 1;
	if (-1 != ptime.myear)
	{
		if (ptime.myear < _tt.tm_year)
		{
			return false;
		}
	}
	if (-1 != ptime.mmon)
	{
		if (ptime.mmon < _tt.tm_mon 
			&& ptime.myear == _tt.tm_year)
		{
			return false;
		}
	}
	if (-1 != ptime.mday)
	{
		if (ptime.mday < _tt.tm_mday
			&& ptime.myear == _tt.tm_year
			&& ptime.mmon == _tt.tm_mon)
		{
			return false;
		}
	}
	if (-1 != ptime.mhour)
	{
		if (ptime.mhour < _tt.tm_hour
			&& ptime.mday == _tt.tm_mday
			&& ptime.myear == _tt.tm_year
			&& ptime.mmon == _tt.tm_mon)
		{
			return false;
		}
	}
	if (-1 != ptime.mmin)
	{
		if (ptime.mmin < _tt.tm_min
			&& ptime.mhour == _tt.tm_hour
			&& ptime.mday == _tt.tm_mday
			&& ptime.myear == _tt.tm_year
			&& ptime.mmon == _tt.tm_mon)
		{
			return false;
		}
	}
	return true;
};
//filter old time
bool checkFutureTime(pyfree::ExceptTime et_)
{
	time_t _t = time(NULL);
	struct tm _tt;
#ifdef WIN32
	localtime_s(&_tt, &_t);//系统本地时间
#else
	localtime_r(&_t, &_tt);//
#endif
	_tt.tm_year += 1900;
	_tt.tm_mon += 1;
	if (-1 != et_.myear)
	{
		if (et_.myear < _tt.tm_year)
		{
			return false;
		}
	}
	if (-1 != et_.mmon)
	{
		if (et_.mmon < _tt.tm_mon 
			&& et_.myear == _tt.tm_year)
		{
			return false;
		}
	}
	if (-1 != et_.mday)
	{
		if (et_.mday < _tt.tm_mday 
			&& et_.myear == _tt.tm_year
			&& et_.mmon == _tt.tm_mon)
		{
			return false;
		}
	}
	return true;
};

void init_tv_atts(std::map<std::string, std::string> &atts_)
{
	atts_["startHour"] = "";
	atts_["endHour"] = "";
	atts_["startMin"] = "";
	atts_["endMin"] = "";
};

void read_tv_atts(pyfree::TVProperty &tv_,std::map<std::string, std::string> atts_)
{
	tv_.startHour = 0;
	if (!atts_["startHour"].empty())
	{
		tv_.startHour = atoi(atts_["startHour"].c_str());
		if (tv_.startHour > 23)
		{
			tv_.startHour = 23;
		}
	}
	tv_.endHour = 24;
	if (!atts_["endHour"].empty())
	{
		tv_.endHour = atoi(atts_["endHour"].c_str());
		if (tv_.endHour > 23)
		{
			tv_.endHour = 23;
		}
	}
	//
	tv_.startMin = 0;
	if (!atts_["startMin"].empty())
	{
		tv_.startMin = atoi(atts_["startMin"].c_str());
		if (tv_.startMin > 59)
		{
			tv_.startMin = 59;
		}
	}
	tv_.endMin = 0;
	if (!atts_["endMin"].empty())
	{
		tv_.endMin = atoi(atts_["endMin"].c_str());
		if (tv_.endMin > 59)
		{
			tv_.endMin = 59;
		}
	}
	tv_.allStartMin = 60 * tv_.startHour + tv_.startMin;
	tv_.allEndMin = 60 * tv_.endHour + tv_.endMin;
	// Print_NOTICE("\ntv_atts:\nstartHour:%d\nstartMin:%d\nendHour:%d\nendMin:%d\r\n"
	// 	,tv_.startHour
	// 	,tv_.startMin
	// 	,tv_.endHour
	// 	,tv_.endMin);
};

void init_cp_atts(std::map<std::string, std::string> &atts_)
{
	atts_["cid"] = "";
	atts_["devID"] = "";
	atts_["devType"] = "";
	atts_["pID"] = "";
	atts_["pType"] = "";
	atts_["value"] = "";
	atts_["compare"] = "";
	atts_["ValChange"] = "";
};

void read_cp_atts(pyfree::PlanCondition &_planConD, std::map<std::string, std::string> atts_)
{
	_planConD.cID = atoi(atts_["cid"].c_str());
	_planConD.devID = atoll(atts_["devID"].c_str());
	_planConD.devType = (pyfree::DevType)atoi(atts_["devType"].c_str());
	_planConD.pID = atoi(atts_["pID"].c_str());
	_planConD.pType = (pyfree::PType)atoi(atts_["pType"].c_str());
	_planConD.val = static_cast<float>(atof(atts_["value"].c_str()));
	_planConD.compare = (pyfree::CompareType)atoi(atts_["compare"].c_str());
	_planConD.ValChange = true;
	if (!atts_["ValChange"].empty())
	{
		_planConD.ValChange = atoi(atts_["ValChange"].c_str()) > 0 ? true : false;
	}
	// Print_NOTICE("\ncp_atts:\ncid:%d\ndevID:%lld\ndevType:%d\npID:%u\npType:%d\nvalue:%f\ncompare:%d\nValChange:%d\r\n"
	// 	,_planConD.cID
	// 	,_planConD.devID
	// 	,_planConD.devType
	// 	,_planConD.pID
	// 	,_planConD.pType
	// 	,_planConD.val
	// 	,_planConD.compare
	// 	,_planConD.ValChange);
};

void init_cmd_atts(std::map<std::string, std::string> &atts_)
{
	atts_["devID"] = "";
	atts_["devType"] = "";
	atts_["pID"] = "";
	atts_["pType"] = "";
	atts_["exeType"] = "";
	atts_["value"] = "";
	atts_["waitT"] = "";
	atts_["timedelay"] = "";
	atts_["desc"] = "";
	atts_["dDevID"] = "";
	atts_["dPID"] = "";
	atts_["vtime"] = "";
};

void read_cmd_atts(pyfree::PlanCMD &_plancmd,std::map<std::string, std::string> atts_)
{
	_plancmd.devID = atoll(atts_["devID"].c_str());
	_plancmd.devType = (pyfree::DevType)atoi(atts_["devType"].c_str());
	_plancmd.pID = atoi(atts_["pID"].c_str());
	_plancmd.pType = (pyfree::PType)atoi(atts_["pType"].c_str());
	_plancmd.exeType = (pyfree::ExeType)atoi(atts_["exeType"].c_str());
	_plancmd.val = static_cast<float>(atof(atts_["value"].c_str()));
	_plancmd.waitT = atoi(atts_["waitT"].c_str());
	_plancmd.timedelay = atoi(atts_["timedelay"].c_str());
	_plancmd.desc = atts_["desc"];
//#ifdef WIN32
	// _plancmd.desc = UTF_82ASCII(_plancmd.desc);//当前是监控服务独占的应用需求
//#endif
	//
	_plancmd.dDevID = 0;
	if (!atts_["dDevID"].empty())
	{
		_plancmd.dDevID = atoll(atts_["dDevID"].c_str());
	}
	_plancmd.dPID = 0;
	if (!atts_["dPID"].empty())
	{
		_plancmd.dPID = static_cast<unsigned int>(atoi(atts_["dPID"].c_str()));
	}
	_plancmd.vtime = 5;
	if (!atts_["vtime"].empty())
	{
		_plancmd.vtime = static_cast<unsigned int>(atoi(atts_["vtime"].c_str()));
		if (_plancmd.vtime < 5)
		{
			_plancmd.vtime = 5;
		}
	}
	// Print_NOTICE("\ncmd_atts:\ndevID:%lld\ndevType:%d\npID:%u\npType:%d\nexeType:%d\nvalue:%f\nwaitT:%d\ntimedelay:%u\n"
	// 	"desc:%s\ndDevID:%lld\ndPID:%u\nvtime:%u\r\n"
	// 	,_plancmd.devID
	// 	,_plancmd.devType
	// 	,_plancmd.pID
	// 	,_plancmd.pType
	// 	,_plancmd.exeType
	// 	,_plancmd.val
	// 	,_plancmd.waitT
	// 	,_plancmd.timedelay
	// 	,_plancmd.desc.c_str()
	// 	,_plancmd.dDevID
	// 	,_plancmd.dPID
	// 	,_plancmd.vtime);
};

void pyfree::readPlans(std::vector<Plan> &plans, std::string xml_)
{
	try
	{
		acl::string buf;
		if (acl::ifstream::load(xml_.c_str(), &buf) == false)
		{
			Print_WARN("load %s error %s\r\n", xml_.c_str(), acl::last_serror());
			return;
		}

		acl::xml1 xml;
		xml.update(buf);

		acl::xml_node* node 	= &(xml.get_root());
		acl::xml_node* child 	= node->first_child();
        while (child)
		{
			if(0==strcmp(child->tag_name(), "plans"))
			{
				acl::xml_node* pnode	= child;
				acl::xml_node* pchild 	= pnode->first_child();
				while (pchild)
				{
					if(0==strcmp(pchild->tag_name(), "plan"))
					{
						std::map<std::string, std::string> atts_;
						init_plan_atts(atts_);

						getAtt(pchild, atts_);
						Plan plan_;
						read_plan_atts(plan_.planAtt, atts_);

						acl::xml_node* cnode	= pchild;
						acl::xml_node* cchild 	= cnode->first_child();
						while (cchild)
						{
							if(0==strcmp(cchild->tag_name(), "DTime"))
							{
								atts_.clear();
								init_dt_atts(atts_);

								getAtt(cchild, atts_);
								pyfree::PlanTime ptime;
								read_dt_atts(ptime, atts_);

								acl::xml_node* enode	= cchild;
								acl::xml_node* echild 	= enode->first_child();
								while (echild)
								{
									if(0==strcmp(echild->tag_name(), "ExceptTime"))
									{
										atts_.clear();
										init_et_atts(atts_);

										getAtt(echild, atts_);
										ExceptTime et_;
										read_et_atts(et_, atts_);
										//
										if (checkFutureTime(et_)) {
											ptime.exceptList.push_back(et_);
										}
									}
									echild = enode->next_child();
								}
								if (checkFutureTimePlan(ptime))
								{
									plan_.ptimes.push_back(ptime);
								}
							}
							if(0==strcmp(cchild->tag_name(), "Interval"))
							{
								atts_.clear();
								atts_["sleepT"] = "";
								getAtt(cchild, atts_);
								plan_.psleep.interval = atoi(atts_["sleepT"].c_str());
								if (plan_.psleep.interval < 1)
								{
									plan_.psleep.interval = 1;
								}
								plan_.psleep.tt = 10;
								//
								acl::xml_node* enode	= cchild;
								acl::xml_node* echild 	= enode->first_child();
								while (echild)
								{
									if(0==strcmp(echild->tag_name(), "TV"))
									{
										atts_.clear();
										init_tv_atts(atts_);
										getAtt(echild, atts_);

										TVProperty tv_;
										read_tv_atts(tv_, atts_);
										//
										plan_.timeVS.push_back(tv_);
									}
									echild = enode->next_child();
								}
							}
							if(0==strcmp(cchild->tag_name(), "condition"))
							{
								acl::xml_node* enode	= cchild;
								acl::xml_node* echild 	= enode->first_child();
								while (echild)
								{
									if(0==strcmp(echild->tag_name(), "TV"))
									{
										atts_.clear();
										init_tv_atts(atts_);
										getAtt(echild, atts_);

										TVProperty tv_;
										read_tv_atts(tv_, atts_);
										//
										plan_.timeVS.push_back(tv_);
									}
									if(0==strcmp(echild->tag_name(), "p"))
									{
										atts_.clear();
										init_cp_atts(atts_);
										//
										getAtt(echild, atts_);
										pyfree::PlanCondition planConD_;
										read_cp_atts(planConD_, atts_);
										//
										plan_.plancons.push_back(planConD_);
									}
									echild = enode->next_child();
								}
							}
							if(0==strcmp(cchild->tag_name(), "StartPlan"))
							{
								atts_.clear();
								atts_["timedelay"] = "";
								getAtt(cchild, atts_);
								//
								plan_.pFStart.execFlag = false;
								plan_.pFStart.waitSec = 10;
								if (!atts_["timedelay"].empty())
								{
									plan_.pFStart.waitSec = atoi(atts_["timedelay"].c_str());
									if (plan_.pFStart.waitSec < 10)
										plan_.pFStart.waitSec = 10;
								}
								//
								acl::xml_node* enode	= cchild;
								acl::xml_node* echild 	= enode->first_child();
								while (echild)
								{
									if(0==strcmp(echild->tag_name(), "TV"))
									{
										atts_.clear();
										init_tv_atts(atts_);
										getAtt(echild, atts_);

										TVProperty tv_;
										read_tv_atts(tv_, atts_);
										//
										plan_.timeVS.push_back(tv_);
									}
									echild = enode->next_child();
								}
							}
							if(0==strcmp(cchild->tag_name(), "CMD"))
							{
								atts_.clear();
								init_cmd_atts(atts_);
								//
								getAtt(cchild, atts_);

								pyfree::PlanCMD plancmd_;
								read_cmd_atts(plancmd_, atts_);
								//
								plan_.planCmds.push_back(plancmd_);
							}
							cchild = cnode->next_child();
						}
						plans.push_back(plan_);
					}
					pchild = pnode->next_child();
				}
			}
			child = node->next_child();
		}
		CLogger::createInstance()->Log(MsgInfo,"success for read %s!\n",xml_.c_str());
	}
	catch (...)
	{
		CLogger::createInstance()->Log(MsgError,
			"read xml file[%s] error: %s %s %d"
			", please check the file format and code!"
			, xml_.c_str()
			,__FILE__, __FUNCTION__, __LINE__);
	}
};

void init_alarmcmd_atts(std::map<std::string, std::string> &alarmcmd_atts)
{
	alarmcmd_atts["id"] = "";
	alarmcmd_atts["type"] = "";
	alarmcmd_atts["level"] = "";
	alarmcmd_atts["way"] = "";
	alarmcmd_atts["desc"] = "";
	alarmcmd_atts["devID"] = "";
	alarmcmd_atts["pID"] = "";
	alarmcmd_atts["DayAlarmCountLimit"] = "";
	alarmcmd_atts["AlarmInterval"] = "";
};

void read_alarmcmd_atts(pyfree::AlarmCMD &alarmCmd_, std::map<std::string, std::string> alarmcmd_atts)
{
	alarmCmd_.ID = atol(alarmcmd_atts["id"].c_str());
	alarmCmd_.type = (pyfree::EventType)atoi(alarmcmd_atts["type"].c_str());
	alarmCmd_.level = (pyfree::EventLevel)atoi(alarmcmd_atts["level"].c_str());
	alarmCmd_.way = (pyfree::EventWay)atoi(alarmcmd_atts["way"].c_str());
	alarmCmd_.desc = alarmcmd_atts["desc"];
//#ifdef WIN32
	// alarmCmd_.desc = UTF_82ASCII(alarmCmd_.desc);//当前是监控服务独占的应用需求
//#endif
	alarmCmd_.time = "";
	alarmCmd_.devID = 0;//
	if (!alarmcmd_atts["devID"].empty()) 
	{
		alarmCmd_.devID = atol(alarmcmd_atts["devID"].c_str());
	}
	alarmCmd_.devDesc = "undef";
	alarmCmd_.pID = 0;
	if (!alarmcmd_atts["pID"].empty()) 
	{
		alarmCmd_.pID = atol(alarmcmd_atts["pID"].c_str());
	}
	alarmCmd_.pDesc = "undef";
	alarmCmd_.dayFlag = 0;
	alarmCmd_.dayAlarmCount = 0;
	alarmCmd_.dayAlarmCountLimit = 3;
	if (!alarmcmd_atts["DayAlarmCountLimit"].empty())
	{
		alarmCmd_.dayAlarmCountLimit = atoi(alarmcmd_atts["DayAlarmCountLimit"].c_str());
		if (alarmCmd_.dayAlarmCountLimit < 1)
		{
			alarmCmd_.dayAlarmCountLimit = 1;
		}
		if (alarmCmd_.dayAlarmCountLimit > 10)
		{
			alarmCmd_.dayAlarmCountLimit = 10;
		}
	}
	alarmCmd_.alarmTime = 0;
	alarmCmd_.alarmInterval = 60;
	if (!alarmcmd_atts["AlarmInterval"].empty())
	{
		alarmCmd_.alarmInterval = atoi(alarmcmd_atts["AlarmInterval"].c_str());
		if (alarmCmd_.dayAlarmCountLimit < 1)
		{
			alarmCmd_.dayAlarmCountLimit = 1;
		}
		if (alarmCmd_.dayAlarmCountLimit > 3600)
		{
			alarmCmd_.dayAlarmCountLimit = 3600;
		}
	}
	// Print_NOTICE("\nalarmcmd_atts:\nid:%lu\ntype:%d\nlevel:%d\nway:%d\ndesc:%s\n"
	// 	"devID:%lu\npID:%lu\nDayAlarmCountLimit:%d\nAlarmInterval:%d\r\n"
	// 	,alarmCmd_.ID
	// 	,alarmCmd_.type
	// 	,alarmCmd_.level
	// 	,alarmCmd_.way
	// 	,alarmCmd_.desc.c_str()
	// 	,alarmCmd_.devID
	// 	,alarmCmd_.pID
	// 	,alarmCmd_.dayAlarmCountLimit
	// 	,alarmCmd_.alarmInterval);
};

void pyfree::readAlarms(std::vector<Alarm> &alarms, std::string xml_)
{
	try
	{
		acl::string buf;
		if (acl::ifstream::load(xml_.c_str(), &buf) == false)
		{
			Print_WARN("load %s error %s\r\n", xml_.c_str(), acl::last_serror());
			return;
		}

		acl::xml1 xml;
		xml.update(buf);

		acl::xml_node* node 	= &(xml.get_root());
		acl::xml_node* child 	= node->first_child();
        while (child)
		{
			if(0==strcmp(child->tag_name(), "plans"))
			{
				acl::xml_node* pnode	= child;
				acl::xml_node* pchild 	= pnode->first_child();
				while (pchild)
				{
					if(0==strcmp(pchild->tag_name(), "plan"))
					{
						std::map<std::string, std::string> atts_;
						init_plan_atts(atts_);

						getAtt(pchild, atts_);
						pyfree::Alarm alarm_;
						read_plan_atts(alarm_.planAtt, atts_);

						acl::xml_node* cnode	= pchild;
						acl::xml_node* cchild 	= cnode->first_child();
						while (cchild)
						{
							if(0==strcmp(cchild->tag_name(), "DTime"))
							{
								atts_.clear();
								init_dt_atts(atts_);

								getAtt(cchild, atts_);
								pyfree::PlanTime ptime;
								read_dt_atts(ptime, atts_);

								//添加特定日期排除功能
								acl::xml_node* enode	= cchild;
								acl::xml_node* echild 	= enode->first_child();
								while (echild)
								{
									if(0==strcmp(echild->tag_name(), "ExceptTime"))
									{
										atts_.clear();
										init_et_atts(atts_);

										getAtt(echild, atts_);
										ExceptTime et_;
										read_et_atts(et_, atts_);
										//
										if (checkFutureTime(et_)) {
											ptime.exceptList.push_back(et_);
										}
									}
									echild = enode->next_child();
								}
								if (checkFutureTimePlan(ptime))
								{
									alarm_.ptimes.push_back(ptime);
								}
							}
							if(0==strcmp(cchild->tag_name(), "Interval"))
							{
								atts_.clear();
								atts_["sleepT"] = "";
								getAtt(cchild, atts_);
								alarm_.psleep.interval = atoi(atts_["sleepT"].c_str());
								if (alarm_.psleep.interval < 1)
								{
									alarm_.psleep.interval = 1;
								}
								alarm_.psleep.tt = 10;
								//
								acl::xml_node* enode	= cchild;
								acl::xml_node* echild 	= enode->first_child();
								while (echild)
								{
									if(0==strcmp(echild->tag_name(), "TV"))
									{
										atts_.clear();
										init_tv_atts(atts_);
										getAtt(echild, atts_);

										TVProperty tv_;
										read_tv_atts(tv_, atts_);
										//
										alarm_.timeVS.push_back(tv_);
									}
									echild = enode->next_child();
								}
							}
							if(0==strcmp(cchild->tag_name(), "condition"))
							{
								acl::xml_node* enode	= cchild;
								acl::xml_node* echild 	= enode->first_child();
								while (echild)
								{
									if(0==strcmp(echild->tag_name(), "TV"))
									{
										atts_.clear();
										init_tv_atts(atts_);
										getAtt(echild, atts_);

										TVProperty tv_;
										read_tv_atts(tv_, atts_);
										//
										alarm_.timeVS.push_back(tv_);
									}
									if(0==strcmp(echild->tag_name(), "p"))
									{
										atts_.clear();
										init_cp_atts(atts_);
										//
										getAtt(echild, atts_);
										pyfree::PlanCondition planConD_;
										read_cp_atts(planConD_, atts_);
										//
										alarm_.plancons.push_back(planConD_);
									}
									echild = enode->next_child();
								}
							}
							if(0==strcmp(cchild->tag_name(), "StartPlan"))
							{
								atts_.clear();
								atts_["timedelay"] = "";
								getAtt(cchild, atts_);
								//
								alarm_.pFStart.execFlag = false;
								alarm_.pFStart.waitSec = 10;
								if (!atts_["timedelay"].empty())
								{
									alarm_.pFStart.waitSec = atoi(atts_["timedelay"].c_str());
									if (alarm_.pFStart.waitSec < 10)
										alarm_.pFStart.waitSec = 10;
								}
								//
								acl::xml_node* enode	= cchild;
								acl::xml_node* echild 	= enode->first_child();
								while (echild)
								{
									if(0==strcmp(echild->tag_name(), "TV"))
									{
										atts_.clear();
										init_tv_atts(atts_);
										getAtt(echild, atts_);

										TVProperty tv_;
										read_tv_atts(tv_, atts_);
										//
										alarm_.timeVS.push_back(tv_);
									}
									echild = enode->next_child();
								}
							}
							if(0==strcmp(cchild->tag_name(), "CMD"))
							{
								atts_.clear();
								init_alarmcmd_atts(atts_);
								//
								getAtt(cchild, atts_);

								AlarmCMD alarm_cmd;
								read_alarmcmd_atts(alarm_cmd, atts_);
								//
								alarm_.alarmCmds.push_back(alarm_cmd);
							}
							cchild = cnode->next_child();
						}
						alarms.push_back(alarm_);
					}
					pchild = pnode->next_child();
				}
			}
			child = node->next_child();
		}
		CLogger::createInstance()->Log(MsgInfo,"success for read %s!\n",xml_.c_str());
	}
	catch (...)
	{
		CLogger::createInstance()->Log(MsgError,
			"read xml file[%s] error: %s %s %d"
			", please check the file format and code!"
			, xml_.c_str()
			, __FILE__, __FUNCTION__, __LINE__);
	}
};

