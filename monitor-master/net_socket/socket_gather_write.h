#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_GATHER_WRITE_H_
#define _SOCKET_GATHER_WRITE_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_gather_write.h
  *File Mark       : 
  *Summary         : 
  *该线程向指定客户端(采控服务/中控服務)写入数据,将数据协议编码并序列化
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "datadef.h"
#include "queuedata_single.h"

class SocketPrivate_ACL;

class SocketGatherWrite : public acl::thread
{
public:
	SocketGatherWrite(void);
	~SocketGatherWrite(void);
  /**
	 * 设置通信接口指针
	 * @param socket_acl_ {SocketPrivate_ACL* } 通信接口,基于acl库实现
	 * @return {void}
	 */
	void setPrivateDataPtr(SocketPrivate_ACL *socket_acl_);
	void* run();
private:
	int getBuffer(unsigned long long &_ipInt, unsigned char* _buf);
private:
	bool running;
	SocketPrivate_ACL *socket_acl;
	QueueDataSingle<DataToGather> *socket_write_queue;
};

#endif