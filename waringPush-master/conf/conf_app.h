#ifndef _CONF_APP_H_
#define _CONF_APP_H_
/***********************************************************************
  *Copyright 2020-06-13, pyfree
  *
  *File Name       : conf_app.h
  *File Mark       : 
  *Summary         : 程序配置信息
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>
#include <map>
#include <vector>

namespace pyfree
{

struct SysAssistConf
{
	SysAssistConf()
	: diskSymbol('D')
    , freeSizeLimit(10000)
    , dayForLimit(7)
    , gLogDir("log")
	, local_ip("127.0.0.1")
	, local_port(60005)
	{

	};
	char diskSymbol;		//信息存储盘符
	int freeSizeLimit;		//存储路径所在磁盘剩余空间(MB)
	int dayForLimit;		//日志保留最低天数
	std::string gLogDir;	//日志输出目录
	std::string local_ip;	//udp地址
	int	local_port;			//udp端口
};

struct MsgWaring
{
	MsgWaring()
	: msgWaringFunc(false)
    , WEAccessKeyId("LTAIWGIhMW9lfJNH")
    , WEAccessKeySecret("aBbus0elERjdtScn6TgxzZhaNilYjC")
    , WEPhoneNumbers("13726272065")
    , WESignName("pyfree_msg_test")
    , WETemplateCode("SMS_130929354")
    , WEStartTime(0)
    , WEEndTime(1440)
	{

	};
    bool msgWaringFunc;			//短信通知功能开关
	std::string WEAccessKeyId;		//短信云服务key
	std::string WEAccessKeySecret;	//短信云服务密钥
	std::string WEPhoneNumbers;		//短信推送手机号码,逗号分隔多个号码
	std::string WESignName;			//短信签名
	std::string WETemplateCode;		//短信模板ID
    /*
	*单位分钟[0~1440],一天内取得时间范围
	*当WEStartTime>WEEndTime时间段跨天
	*/
	int WEStartTime;				//开始时间(分钟，0~1440)
	int WEEndTime;					//结束时间(分钟)
};


struct Email_User_To
{
	std::string email_user;
	std::string email_addr;
};

struct Email_User_From : public Email_User_To
{
	std::string email_key;
	std::string email_title;
};

struct Email_server
{
	Email_server() 
		: email_host("smtp.163.com"),email_port(25)
	{};
	std::string email_host;
	int email_port;
};

struct MailWaring
{
	MailWaring()
	: mailWaringFunc(false)
	, startTime(0)
	, endTime(1440)
	, day_send_limit(10)
	, day_send_count(0)
	{

	};
    bool mailWaringFunc;			//邮件功能开关(暂无实现)
	unsigned int startTime;			//
	unsigned int endTime;			//
	unsigned int day_send_limit;	//
	unsigned int day_send_count;	//
	Email_server email_srv;
	Email_User_From email_from;
	std::vector<Email_User_To> email_to;
};

//后续需要重构,将各个功能模块涉及参数结构化,并配置文件(xml)里作为一个子节点集写入
struct AppConf
{
	AppConf()	
	{

	};
    //系统辅助功能配置
    SysAssistConf saconf;   //
	//短信
    MsgWaring msgwar;             //
	//邮件
	MailWaring mailwar;
};
};

#endif