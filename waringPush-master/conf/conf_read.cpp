#include "conf_read.h"

#include <map>
#include <fstream>
#include <time.h>

#include "pfunc.h"
#include "Log.h"
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

void read_SysAssistConf(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::SysAssistConf &saconf)
{
	while (pchild){
		if (0==strcmp("DiskSymbol",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (1 == strlen(val_))
				saconf.diskSymbol = val_[0];
		}
		//
		if (0==strcmp("FreeSizeLimit",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.freeSizeLimit = atoi(val_);
			if (saconf.freeSizeLimit < 1000)
				saconf.freeSizeLimit = 1000;
		}
		//
		if (0==strcmp("DayForLimit",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.dayForLimit = atoi(val_);
		}
		if (0==strcmp("LogDir",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.gLogDir = std::string(val_);
		}
		if (0==strcmp("LocalIp",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.local_ip = std::string(val_);
		}
		if (0==strcmp("LocalPort",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				saconf.local_port = atoi(val_);
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nSysAssistConf:\nDiskSymbol:%c\nFreeSizeLimit:%d\nDayForLimit:%d\n"
	// 	"LogDir:%s\nLocalIp:%s\nLocalPort:%d\r\n"
	// 	,saconf.diskSymbol
	// 	,saconf.freeSizeLimit
	// 	,saconf.dayForLimit
	// 	,saconf.gLogDir.c_str()
	// 	,saconf.local_ip.c_str()
	// 	,saconf.local_port);
}

void read_MsgWaring(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::MsgWaring &msgwar)
{
	while (pchild){
		///////////////
		if (0==strcmp("MsgWaringFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				msgwar.msgWaringFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("WEAccessKeyId",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				msgwar.WEAccessKeyId = std::string(val_);
		}
		if (0==strcmp("WEAccessKeySecret",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				msgwar.WEAccessKeySecret = std::string(val_);
		}
		if (0==strcmp("WEPhoneNumbers",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				msgwar.WEPhoneNumbers = std::string(val_);
		}
		if (0==strcmp("WESignName",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				msgwar.WESignName = std::string(val_);
		}
		if (0==strcmp("WETemplateCode",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				msgwar.WETemplateCode = std::string(val_);
		}
		if (0==strcmp("WEStartTime",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				msgwar.WEStartTime = atoi(val_);
			if (msgwar.WEStartTime < 0) 
			{
				msgwar.WEStartTime = 0;
			}
			if (msgwar.WEStartTime >1440) 
			{
				msgwar.WEStartTime = 1440;
			}
		}
		if (0==strcmp("WEEndTime",pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if (strlen(val_)>0)
				msgwar.WEEndTime = atoi(val_);
			if (msgwar.WEEndTime < 0) 
			{
				msgwar.WEEndTime = 0;
			}
			if (msgwar.WEEndTime >1440) 
			{
				msgwar.WEEndTime = 1440;
			}
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nMsgWaring:\nMsgWaringFunc:%d\nWEAccessKeyId:%s\nWEAccessKeySecret:%s\nWEPhoneNumbers:%s\n:WESignName:%s\n"
	// 	"WETemplateCode:%s\nWEStartTime:%d\nWEEndTime:%d\r\n"
	// 	,msgwar.msgWaringFunc
	// 	,msgwar.WEAccessKeyId.c_str()
	// 	,msgwar.WEAccessKeySecret.c_str()
	// 	,msgwar.WEPhoneNumbers.c_str()
	// 	,msgwar.WESignName.c_str()
	// 	,msgwar.WETemplateCode.c_str()
	// 	,msgwar.WEStartTime
	// 	,msgwar.WEEndTime);
}

void read_MailWaring(acl::xml_node* pnode,acl::xml_node* pchild,pyfree::MailWaring &mailwar)
{
	while (pchild){
		if (0==strcmp("MailWaringFunc" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				mailwar.mailWaringFunc = atoi(val_) > 0 ? true : false;
		}
		if (0==strcmp("DaySendLimit" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				mailwar.day_send_limit = static_cast<unsigned int>(atoi(val_));
		}
		if (0==strcmp("MailSrv" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				mailwar.email_srv.email_host = std::string(val_);
		}
		if (0==strcmp("MailPort" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0)			
				mailwar.email_srv.email_port = atoi(val_);
		}
		if (0==strcmp("MailFrom" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0){			
				std::string from_desc = std::string(val_);
				std::vector<std::string> desc_list;
				if(pyfree::string_divide(desc_list,from_desc,","))
				{
					if(4==desc_list.size())
					{
						mailwar.email_from.email_user = desc_list[0];
						mailwar.email_from.email_title = desc_list[1];
						mailwar.email_from.email_addr = desc_list[2];
						mailwar.email_from.email_key = desc_list[3];
					}
				}
			}
		}
		if (0==strcmp("MailTo" , pchild->tag_name())) 
		{
			const char* val_ = pchild->text();
			if(strlen(val_)>0){			
				std::string from_to = std::string(val_);
				std::vector<std::string> desc_list;
				if(pyfree::string_divide(desc_list,from_to,";"))
				{
					for(std::vector<std::string>::iterator it = desc_list.begin();it!=desc_list.end();++it)
					{
						std::vector<std::string> user_list;
						if(pyfree::string_divide(user_list,*it,","))
						{
							if(2==user_list.size())
							{
								pyfree::Email_User_To user_to;
								user_to.email_user = user_list[0];
								user_to.email_addr = user_list[1];
								mailwar.email_to.push_back(user_to);
							}
						}
					}	
				}
			}
		}
		pchild = pnode->next_child();
	}
	// Print_NOTICE("\nMailWaring:\nMailWaringFunc:%d\r\n"
	// 	,mailwar.mailWaringFunc);
}

//
void pyfree::readAppConf(AppConf &conf, std::string xml_ )
{
	try
	{
        acl::string buf;
		if (acl::ifstream::load(xml_.c_str(), &buf) == false)
		{
			Print_WARN("load %s error %s\r\n", xml_.c_str(), acl::last_serror());
			return;
		}

		acl::xml1 xml;
		xml.update(buf);

		acl::xml_node* node 	= &(xml.get_root());
		acl::xml_node* child 	= node->first_child();
        while (child)
		{
			if(0==strcmp(child->tag_name(), "appconf"))
			{
				node	= child;
				child 	= node->first_child();
				while (child)
				{
					if (0==strcmp("SysAssistConf",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_SysAssistConf(pnode,pchild,conf.saconf);
					}
                    if (0==strcmp("MsgWaring",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_MsgWaring(pnode,pchild,conf.msgwar);
					}
                    if (0==strcmp("MailWaring",child->tag_name())) 
					{
						acl::xml_node* pnode 	= child;
						acl::xml_node* pchild 	= pnode->first_child();
						read_MailWaring(pnode,pchild,conf.mailwar);
					}
					child = node->next_child();
				}
				break;
			}
			child = node->next_child();
		}
		CLogger::createInstance()->Log(MsgInfo,"success for read %s!\n",xml_.c_str());
	}
	catch (...)
	{
		CLogger::createInstance()->Log(MsgError,
			"read xml file[%s] error: %s %s %d,please check the file format and code!\n"
			, xml_.c_str(), __FILE__, __FUNCTION__, __LINE__);
	}
};

void pyfree::writeAppConf(const AppConf conf, std::string xml_)
{
	try {
		
	}
	catch (...)
	{
		CLogger::createInstance()->Log(MsgError
			, "write xml file[%s] error[%s]: %s %s %d"
			", please check the code!"
			, xml_.c_str()
			,__FILE__, __FUNCTION__, __LINE__);
	}
};
