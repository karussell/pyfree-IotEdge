#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _EMAIL_H_
#define _EMAIL_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : email.h
  *File Mark       :
  *Summary         : 电子邮件接口
  *
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

#ifdef WIN32
#include <windows.h>
#include <WinSock.h>
#else
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#endif
#include <stdio.h>
#include <iostream>
#include <string>

#include "dtypedef.h"
#include "conf_war.h"
#include "conf_app.h"

class Email
{
public:
    Email(/* args */);
    ~Email();
    /**
	 * 根据邮件配置信息指定的时间范围判定当前是否可以发送邮件
	 * @return {bool}
	 */
    bool isMapMailTime();
    /**
	 * 将告警事件生成邮件发送出去
     * @param efw {EventForWaring} 告警信息
	 * @return {void}
	 */
    void send_email(EventForWaring efw);
private:
    /**
	 * 将指定内容以邮件方式发送
     * @param comment {string} 发送内容
	 * @return {void}
	 */
    void send_email(std::string comment);
    /**
	 * 真正发送邮件函数
     * @param emailTo {char*} 接收人电子邮箱
     * @param body {const char*} 发送内容
     * @param emailFrom {char*} 发送人电子邮箱
     * @param emailKey {char*} 发送人电子邮箱密码
	 * @return {void}
	 */
    void sendEmail(char *emailTo, const char *body,char* emailFrom, char* emailKey);
    /**
	 * 打开邮件服务器的TCP Socket连接
     * @param addr {sockaddr*} 服务器网络地址
	 * @return {int} 通道句柄
	 */
    int openEmailSocket(struct sockaddr *addr);
private:
    pyfree::MailWaring email_conf;  //电子邮件相关配置信息
    // std::string area_desc;          //区域描述
};



#endif
