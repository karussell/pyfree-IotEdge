### 使用说明
> 配置  
本测试demo时在win10主机,通过vmware安装了centos7服务器(虚拟机,非桌面版),安装了docker以及docker-compose引擎  

> 部署  
(1)可以提前加载镜像,docker pull centos:7  
(2)构建镜像pyfree及容器、网桥,并执行,运行./build.sh即可,如果没预期拉去镜像centos:7，会先拉取centos:7镜像  
(3)删除镜像pyfree及容器、网桥,运行./remove.sh  
(4)启动或停止容器docker start(stop) gather(monitor)

> 使用  
(1)本demo的网络配置如下:  
   采集软件容器ip地址172.100.100.2  
   调度软件容器ip地址172.100.100.3  
   虚拟机地址192.168.174.138  
   主机地址192.168.174.1,下级采集软件、可视化终端、测试播放器、串口仿真工具部署在win主机  
(2)下级采集软件在demo-project/bgather目录,需要修改gather.xml连接上级采集软件的地址,注意是虚拟机地址，因为虚拟机是容器主机，已经做了网桥    
```
<netport id="3" type="1" ptype="3" name="转发上级采集" ip="192.168.174.138" port="10001" />
```
(3)需要在本目录下的/gather/gather.xml修改网络地址
```
<netport id="1" type="1" ptype="1" name="采集播放器" ip="192.168.174.1" port="10088" />
<!--注意由于采用acl库的tcp-socket通信接口,因此其ip地址也需要配置准确-->
<netport id="2" type="1" ptype="2" name="下级采集" ip="172.100.100.2" port="10001" />
<netport id="3" type="1" ptype="3" name="转发后台" ip="172.100.100.3" port="60002" />
```
```
<!--同样下级采集各信息点的网络地址也要修改-->
<pinfo id="1" type="1" addr="1" ip="192.168.174.1"/>
```
(4)需要在本目录下的/gather/appconf.xml修改网络地址  
```
<!-- 侦听采集端口本地网卡的网络地址-->
<GatherIp>172.100.100.3</GatherIp>
<!-- 侦听采集端的网络端口-->
<GatherPort>60002</GatherPort>
<!-- 转发接口开关 -->
<TranFunc>0</TranFunc>
<!-- 链接第三方ip -->
<TranIP>192.168.174.1</TranIP>
<!-- 链接第三方端口 -->
<TranPort>60003</TranPort>
<!-- 本地客户端的socket接口开关 -->
<LocalSocketFunc>1</LocalSocketFunc>
<!-- 链接本地socket服务接口指定网口的ip -->
<LocalSocketIp>172.100.100.3</LocalSocketIp>
<!-- 链接本地socket服务接口指定端口-->
<LocalSocketPort>60008</LocalSocketPort>
```
(5)测试客户端部署在win主机,demo-project/client_test目录下,需要修改conf.ini的网络连接地址  
```
srvip=192.168.174.138  
```
(6)测试播放器部署在win主机,demo-project/player_test目录下，直接启动即可  

(7) 串口工具部署在win主机,demo-project/tool目录下  
采用"tool/VSPD虚拟串口.zip"串口工具安装并创建一个串口对COM4<->COM5  
采用"tool/mbslave.exe"工具模拟设备端,打开并设置其端口选择COM5,19200 8 N 1  