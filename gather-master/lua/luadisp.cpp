#include "luadisp.h"
#include "Log.h"

luadisp::luadisp(void)
{
	m_pLua = NULL;
}

luadisp::~luadisp(void)
{
	close();
}

bool luadisp::open(const char * path)
{
	try {
		m_pLua = luaL_newstate();
		luaL_openlibs(m_pLua);
		if (!luaL_loadfile(m_pLua, path) && !lua_pcall(m_pLua, 0, 0, 0)) 
		{
			return true;
		}
		else {
			lua_close(m_pLua);
			m_pLua = NULL;
			return false;
		}
	}
	catch (const std::exception& e)
	{
		CLogger::createInstance()->Log(MsgInfo
			, "open lua file(%s) Error[%s]"
			, path
			, e.what());
		return false;
	}
}

bool luadisp::close()
{
	try {
		if (m_pLua) 
		{
			lua_close(m_pLua);
			m_pLua = NULL;
		}
		return true;
	}
	catch (const std::exception& e)
	{
		CLogger::createInstance()->Log(MsgInfo
			, "close lua file Error[%s]"
			, e.what());
		return false;
	}
}

bool luadisp::getTotalCallCMD(std::string &_cmd, std::string &_errmsg, int &_type)
{
	if (!m_pLua) 
	{
		_errmsg = "un't load lua script";
		return false;
	}
	try
	{
		m_lua_Mutex.Lock();
		lua_gc(m_pLua, LUA_GCCOLLECT, 0);
		lua_getglobal(m_pLua, "getTotalCallCMD");
		lua_pcall(m_pLua, 0, 2, 0);
		const char* strMsg = lua_tostring(m_pLua, -2);
		lua_Number rtype = lua_tonumber(m_pLua, -1);
		_cmd = std::string(strMsg);
		_type = static_cast<int>(rtype);
		lua_pop(m_pLua, 1);
		lua_settop(m_pLua, 0);
		m_lua_Mutex.Unlock();
		_errmsg = "get cmd";
		return true;
	}
	catch (const std::exception& e)
	{
		CLogger::createInstance()->Log(MsgInfo
			,"getTotalCallCMD Error[%s]"
			, e.what());
		return false;
	}
}

bool luadisp::getReciveIDVAL(const char* _cmd, std::string &_recives, std::string &_errmsg, std::string SpecificDesc)
{
	if (!m_pLua) 
	{
		_errmsg = "un't load lua script";
		return false;
	}
	try
	{
		int retSize = 0;
		m_lua_Mutex.Lock();
		lua_gc(m_pLua, LUA_GCCOLLECT, 0);
		lua_getglobal(m_pLua, "getReciveIDVAL");
		lua_pushstring(m_pLua, _cmd);//ret cmd
		lua_pushstring(m_pLua, SpecificDesc.c_str());//control cmd
		lua_pcall(m_pLua, 2, 2, 0);
		const char* strMsg = lua_tostring(m_pLua, -2);
		lua_Integer vsize = lua_tointeger(m_pLua, -1);
		if (NULL != strMsg) {
			_recives = std::string(strMsg,strlen(strMsg));
		}
		retSize = static_cast<int>(vsize);
		lua_pop(m_pLua, 1);
		lua_settop(m_pLua, 0);
		m_lua_Mutex.Unlock();
		if (retSize>0 && !_recives.empty())
		{
			_errmsg = "get cmd";
			return true;
		}
		else {
			_errmsg = "get cmd but error count";
			return false;
		}
	}
	catch (const std::exception& e)
	{
		CLogger::createInstance()->Log(MsgInfo
			,"getReciveIDVAL[%s] Error[%s]"
			, _cmd
			, e.what());
		return false;
	}
};

bool luadisp::getResponse(std::string comment_,std::string &cmd_, int &checkType_,std::string &errmsg_
	, unsigned int respType/*=1*/,unsigned int task_id/*=0*/)
{
	if (!m_pLua) 
	{
		errmsg_ = "un't load lua script";
		return false;
	}
	try
	{
		m_lua_Mutex.Lock();
		lua_gc(m_pLua, LUA_GCCOLLECT, 0);
		lua_getglobal(m_pLua, "getResponse");
		lua_pushstring(m_pLua, comment_.c_str());
		lua_pushunsigned(m_pLua, respType);
		lua_pushunsigned(m_pLua, task_id);
		lua_pcall(m_pLua, 3, 2, 0);
		const char* strMsg = lua_tostring(m_pLua, -2);
		lua_Integer bcheck = lua_tointeger(m_pLua, -1);
		if (NULL != strMsg) {
			cmd_ = std::string(strMsg);
		}
		checkType_ = static_cast<int>(bcheck);
		lua_pop(m_pLua, 1);
		lua_settop(m_pLua, 0);
		m_lua_Mutex.Unlock();
		if (checkType_ < 0|| cmd_.empty())
		{
			errmsg_ = "get cmd error";
			printf("cmd:%s\n", cmd_.c_str());
			return false;
		}
		else {
			errmsg_ = "get cmd success";
			return true;
		}
	}
	catch (const std::exception& e)
	{
		CLogger::createInstance()->Log(MsgInfo
			,"getResponse[%s] Error[%s]"
			, comment_.c_str()
			, e.what());
		return false;
	}
	catch (...) {
		CLogger::createInstance()->Log(MsgInfo
			, "getResponse[%s] unkown Error"
			, comment_.c_str());
		return false;
	}
}

bool luadisp::getDownControl(int exetype, int ptype, int addr, int val
	, std::string &_cmd, int &_checkType,std::string &_errmsg
	, unsigned int task_id)
{
	if (!m_pLua) 
	{
		_errmsg = "un't load lua script";
		return false;
	}
	try
	{
		m_lua_Mutex.Lock();
		lua_gc(m_pLua, LUA_GCCOLLECT, 0);
		lua_getglobal(m_pLua, "getDownControl");
		lua_pushinteger(m_pLua, exetype);
		lua_pushinteger(m_pLua, ptype);
		lua_pushinteger(m_pLua, addr);
		lua_pushnumber(m_pLua, val);
		lua_pushunsigned(m_pLua, task_id);
		lua_pcall(m_pLua, 5, 2, 0);
		const char* strMsg = lua_tostring(m_pLua, -2);
		lua_Integer bcheck = lua_tointeger(m_pLua, -1);
		if (NULL != strMsg) {
			_cmd = std::string(strMsg);
		}
		_checkType = static_cast<int>(bcheck);
		lua_pop(m_pLua, 1);
		lua_settop(m_pLua, 0);
		m_lua_Mutex.Unlock();
		if (_checkType < 0|| _cmd.empty())
		{
			_errmsg = "get cmd error";
			printf("cmd:%s\n", _cmd.c_str());
			return false;
		}
		else {
			_errmsg = "get cmd success";
			return true;
		}
	}
	catch (const std::exception& e)
	{
		CLogger::createInstance()->Log(MsgInfo
			,"getDownControl[exetype:%d,ptype:%d,addr:%d,val:%d] Error[%s]"
			, exetype,ptype,addr,val
			, e.what());
		return false;
	}
	catch (...) {
		CLogger::createInstance()->Log(MsgInfo
			, "getDownControl[exetype:%d,ptype:%d,addr:%d,val:%d] unkown Error"
			, exetype, ptype, addr, val);
		return false;
	}
}

