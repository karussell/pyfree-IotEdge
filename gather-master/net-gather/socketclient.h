#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_CLIENT_H_
#define _SOCKET_CLIENT_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socketclient.h
  *File Mark       : 
  *Summary         : 网口TCP/IP通信
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "datadef.h"

class SocketClinet
{
public:
    SocketClinet(std::string ip_, int port_);
    ~SocketClinet();

    bool connect();
    bool isConnect();
    bool disConnect();

    int Read(RDClient &bufs);
	int Read(char* buf, unsigned int size);
	int Write(const char* buf, unsigned int size);
private:
    acl::string addr_;
    acl::socket_stream client;
};




#endif
