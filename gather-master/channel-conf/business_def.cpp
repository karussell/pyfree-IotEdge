#include "business_def.h"

#include "Log.h"
#include "conf_read.h"

namespace GlobalVar{
	extern std::string m_app_conf_file;
	extern std::string logdir;
};

BusinessDef* BusinessDef::instance = NULL;
BusinessDef* BusinessDef::getInstance()
{
	if (NULL == BusinessDef::instance)
	{
		BusinessDef::instance = new BusinessDef();
	}
	return BusinessDef::instance;
};

void BusinessDef::Destroy()
{
	if (NULL != BusinessDef::instance) 
	{
		delete BusinessDef::instance;
		BusinessDef::instance = NULL;
	}
};

BusinessDef::~BusinessDef()
{

};

void BusinessDef::init()
{
	pyfree::readAppConf(appConf,GlobalVar::m_app_conf_file);
	CLogger::createInstance()->Log(MsgInfo
		, "load %s finish!",GlobalVar::m_app_conf_file.c_str());
	GlobalVar::logdir = appConf.gLogDir;
	pyfree::readGathers(commdef,appConf.gatherPath);
	CLogger::createInstance()->Log(MsgInfo
		, "load %s finish!"
		, appConf.gatherPath.c_str());
	pyfree::readPMaps(pmaps,appConf.gmapPath);
	CLogger::createInstance()->Log(MsgInfo
		, "load %s finish!"
		, appConf.gmapPath.c_str());
	for (std::list<pyfree::PMap>::iterator it = pmaps.begin(); it != pmaps.end(); ++it)
	{
		ftmap.insert(KeyObj_FT(it->pfrom.gid,it->pfrom.pid,it->pfrom.ptype), it->pto);
		tfmap.insert(KeyObj_TF(it->pto.ptype,it->pto.pid), it->pfrom);
	}
	if (ftmap.size() != tfmap.size())
	{
		CLogger::createInstance()->Log(MsgWarn
			, "point map config error,the ftmap.size=%d,tfmap=%d,please check your config file map.xml!"
			, ftmap.size(), tfmap.size());
	}
};

bool BusinessDef::getPTO(int _gid, int _pid, pyfree::PType _ptype, pyfree::PTo &_pto)
{
	return ftmap.getVal(KeyObj_FT(_gid, _pid, _ptype),_pto);
};

bool BusinessDef::getPFrom(int _pid, pyfree::PType _ptype, pyfree::PFrom &pf)
{
	return tfmap.getVal(KeyObj_TF(_ptype,_pid), pf);
};

bool BusinessDef::getPOS(std::map<int,LiveData> &tran_infos)
{
	for (std::list<pyfree::PMap>::iterator it = pmaps.begin(); it != pmaps.end(); ++it)
	{
		LiveData item;
		item.type = it->pto.ptype;
		item.value = it->pto.value;
		item.update_time = pyfree::getUsec();
		tran_infos[it->pto.pid] = item;
	}
	return !tran_infos.empty();
}

char BusinessDef::getDiskSymbol()
{
	return appConf.diskSymbol;
};

int BusinessDef::getFreeSizeLimit()
{
	return appConf.freeSizeLimit;
};

std::string BusinessDef::getLogDir()
{
	return appConf.gLogDir;
};

void BusinessDef::outAppConf()
{
	Print_NOTICE("\nAppConf:\nDiskSymbol=%c\nFreeSizeLimit=%d\nGLogDir=%s\nGatherPath=%s\nGmapPath=%s\r\n"
		,appConf.diskSymbol
		,appConf.freeSizeLimit
		,appConf.gLogDir.c_str()
		,appConf.gatherPath.c_str()
		,appConf.gmapPath.c_str());
};

bool BusinessDef::getNetInfoByGID(int gid_,pyfree::NetPort &item)
{
	//根据采集编号获取该采集信道的配置信息
	std::map<int,pyfree::Gather>::iterator itgather = commdef.gathers.find(gid_);
	if(itgather!=commdef.gathers.end())
	{
		std::map<int, pyfree::NetPort>::iterator itp = commdef.netdefs.find(itgather->second.atts.channelid);
		if (itp != commdef.netdefs.end())
		{
			item = itp->second;
			return true;
		}
	}
	return false;
}

bool BusinessDef::getSerialInfoByGID(int gid_,pyfree::SerialPort &item)
{
	//根据采集编号获取该采集信道的配置信息
	std::map<int,pyfree::Gather>::iterator itgather = commdef.gathers.find(gid_);
	if(itgather!=commdef.gathers.end())
	{
		std::map<int, pyfree::SerialPort>::iterator itp = commdef.spdefs.find(itgather->second.atts.channelid);
		if (itp != commdef.spdefs.end())
		{
			item = itp->second;
			return true;
		}
	}
	return false;
}

bool BusinessDef::getProtocolInfoByGID(int gid_,pyfree::ProtocolDef &item)
{
	//根据采集编号获取该采集信道的配置信息
	std::map<int,pyfree::Gather>::iterator itgather = commdef.gathers.find(gid_);
	if(itgather!=commdef.gathers.end())
	{
		std::map<int, pyfree::ProtocolDef>::iterator itp = commdef.protodefs.find(itgather->second.atts.protocolid);
		if (itp != commdef.protodefs.end())
		{
			item = itp->second;
			return true;
		}
	}
	return false;
}

bool BusinessDef::getGatherAttsByID(int gid_,pyfree::GatherAtts &atts_)
{
	//根据采集编号获取该采集信道的配置信息
	std::map<int,pyfree::Gather>::iterator itgather = commdef.gathers.find(gid_);
	if(itgather!=commdef.gathers.end())
	{
		atts_ = itgather->second.atts;
		return true;
	}
	return false;
}

bool BusinessDef::getPoitInfoByID(int gid_,std::list<pyfree::PInfo> &list)
{
	//根据采集编号获取该采集信道的配置信息
	std::map<int,pyfree::Gather>::iterator itgather = commdef.gathers.find(gid_);
	if(itgather!=commdef.gathers.end())
	{
		list = itgather->second.pinfos;
		return true;
	}
	return false;
}

bool BusinessDef::getFPoitInfoByID(int gid_,std::list<pyfree::PFuncInfo> &list)
{
	//根据采集编号获取该采集信道的配置信息
	std::map<int,pyfree::Gather>::iterator itgather = commdef.gathers.find(gid_);
	if(itgather!=commdef.gathers.end())
	{
		list = itgather->second.pfuncinfos;
		return true;
	}
	return false;
}


bool BusinessDef::getNetInfoByTID(int tid_,pyfree::NetPort &item)
{
	//根据采集编号获取该采集信道的配置信息
	std::map<int,pyfree::Transmit>::iterator ittran = commdef.trans.find(tid_);
	if(ittran!=commdef.trans.end())
	{
		std::map<int, pyfree::NetPort>::iterator itp = commdef.netdefs.find(ittran->second.channelid);
		if (itp != commdef.netdefs.end())
		{
			item = itp->second;
			return true;
		}
	}
	return false;
}

bool BusinessDef::getSerialInfoByTID(int tid_,pyfree::SerialPort &item)
{
	//根据采集编号获取该采集信道的配置信息
	std::map<int,pyfree::Transmit>::iterator ittran = commdef.trans.find(tid_);
	if(ittran!=commdef.trans.end())
	{
		std::map<int, pyfree::SerialPort>::iterator itp = commdef.spdefs.find(ittran->second.channelid);
		if (itp != commdef.spdefs.end())
		{
			item = itp->second;
			return true;
		}
	}
	return false;
}

bool BusinessDef::getTranInfoByID(int tid_,pyfree::Transmit &item)
{
	//根据转发编号获取该信道的配置信息
	std::map<int,pyfree::Transmit>::iterator ittran = commdef.trans.find(tid_);
	if(ittran!=commdef.trans.end())
	{
		item = ittran->second;
		return true;
	}
	return false;
}


bool BusinessDef::getProtocolInfoByTID(int tid_,pyfree::ProtocolDef &item)
{
	//根据转发编号获取该信道的配置信息
	std::map<int,pyfree::Transmit>::iterator ittran = commdef.trans.find(tid_);
	if(ittran!=commdef.trans.end())
	{
		std::map<int, pyfree::ProtocolDef>::iterator itp = commdef.protodefs.find(ittran->second.protocolid);
		if (itp != commdef.protodefs.end())
		{
			item = itp->second;
			return true;
		}
	}
	return false;
}

bool BusinessDef::getProtocolTypeByTID(int tid_,int &ptype)
{
	bool ret = false;
	//根据转发编号获取该信道的配置信息
	std::map<int,pyfree::Transmit>::iterator ittran = commdef.trans.find(tid_);
	if(ittran!=commdef.trans.end())
	{
		switch (ittran->second.channeltype)
		{
		case 1://串口只能采用脚本解析
			ptype = 1;
			ret = true;
			break;
		case 2:
		{
			std::map<int, pyfree::NetPort>::iterator itp = commdef.netdefs.find(ittran->second.channelid);
			if (itp != commdef.netdefs.end())
			{
				ptype = itp->second.ptype;
				ret = true;
			}
		} 
			break;
		default:
			break;
		}
	}	
	return ret;
}
