#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PFUNC_STR_H_
#define _PFUNC_STR_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : pfunc_str.h
  *File Mark       : 
  *Summary         : 字符串相关处理函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>
#include <vector>

namespace pyfree
{
/**
	 * 根据指定字符标识分割字符串
	 * @param _strlist {vector} 分割结果容器
	 * @param str {string} 被分割字符串
	 * @param div {string} 分割标识,如{, _ - .}
	 * @return {bool} 分割是否成功
	 */
	bool string_divide(std::vector<std::string> &_strlist, const std::string src, const std::string div);
	/**
	 * 根据指定字符标识连接字符串
	 * @param _strlist {vector} 待连接字符串容器
	 * @param str {string} 连接结果输出的字符串
	 * @param div {string} 连接标识,如{, _ - .}
	 * @return {bool} 连接是否成功
	 */
	bool string_join(const std::vector<std::string> _strlist, std::string &src, const std::string div);
	/**
	 * 在指定字符串查找特定字符串
	 * @param pSrc {const char* } 被查找字符串
	 * @param pDst {unsigned char*} 子字符串
	 * @return {bool} 是否存在
	 */
	bool containStr(const std::string _str, const std::string _strsub);
	/**
	 * 在指定字符串查找特定字符串,必须从字符串头匹配才正确
	 * @param pSrc {const char* } 被查找字符串
	 * @param pDst {unsigned char*} 子字符串
	 * @return {bool} 是否存在
	 */
	bool containStr_start(const std::string _str, const std::string _strsub);
};
#endif
