#include "version.h"
#ifdef WIN32
#define usleep(x) Sleep(x)
#include "strchange.h"
#endif
#include "License.h"
#include "appinfo.h"
#include "File.h"
#include "pfunc.h"
#include "Log.h"
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

namespace GlobalVar {
	extern std::string m_app_conf_file;
};

bool pyfree::LicenseCheck()
{
	bool license_check = true;
#ifdef DEBUG
	license_check = false;
#endif
	bool bRet = !license_check;
	if (!bRet)
	{
		CLicense sn1;
		if (sn1.Serialize("sn.txt", false))
		{
			CLogger::createInstance()->Log(MsgInfo,"license:%s", sn1.ToString().c_str());
			CLicense sn;	// used netcard serial first
			if (sn.Create()&&sn.encrypt())
			{
				bRet = (sn1 == sn);
			}
			if (!bRet)
			{
				sn.SetMacAddrType(1); // used HD serial second
				if (sn.Create()&&sn.encrypt())
				{
					bRet = (sn1 == sn);
				}
			}
		}
	}

	return bRet;
};

void pyfree::versionLog()
{
	char bufVersion[512] = { 0 };
	sprintf(bufVersion
		, "软件简述\n"
		"名称:%s\n"
		"版本:%s.%s\n"
#ifdef DEBUG
		"产品:%sd.exe\n"
#else
		"产品:%s.exe\n"
#endif
		"版权:%s\n"
		"企业:%s\n"
		"地址:%s\n"
		"电话:%s\n"
		"传真:%s\n"
		"网址:%s\n"
		"反馈:%s"
		, STR_VERSION_PRODUCTNAME
		, STR_VERSION_VERSION, STR_BUILD_NUMBER
		, STR_VERSION_FILE
		, STR_VERSION_LEGAL
		, STR_VERSION_COMPANY
		, STR_VERSION_ADDRESS
		, STR_VERSION_TELE
		, STR_VERSION_FAX
		, STR_VERSION_WEB
		, STR_VERSION_DPEMAIL
	);
	std::string desc_version = std::string(bufVersion);
	#ifdef WIN32
	desc_version = pyfree::UTF_82ASCII(desc_version);
	#endif
    CLogger::createInstance()->Log(MsgInfo,"%s",desc_version.c_str());
}

//方便linux启动脚本指定运行参数文件
void pyfree::checkArg(int argc, char* argv[])
{
	if (argc >=2 )
	{
		char m_Arg_c[256] = { 0 };
		sscanf(argv[1], "%s", m_Arg_c);
		printf("arg[1]=\"%s\"", m_Arg_c);
		std::string m_Arg_s = std::string(m_Arg_c);
		std::vector<std::string> m_StrList_vec;
		if (pyfree::string_divide(m_StrList_vec, m_Arg_s, "="))
		{
			if (2 == m_StrList_vec.size())
			{
				if ("--path" == m_StrList_vec.at(0))
				{
					if (pyfree::isExist(m_StrList_vec.at(1)))
					{
						GlobalVar::m_app_conf_file = m_StrList_vec.at(1);
					}
					else {
						CLogger::createInstance()->Log(MsgError
							, "appconf_file(%s) is not exist, check your arg_for_cmd!");
						usleep(1000);
						exit(true);
					}
				}
			}
		}
	}
};