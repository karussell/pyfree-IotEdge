#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _UARTC_H_
#define _UARTC_H_
/***********************************************************************
  *Copyright 2020-04-06, pyfree
  *
  *File Name       : uart_linux.h
  *File Mark       : 
  *Summary         : 
  *base64编码相关函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <termios.h>

namespace pyfree
{
	/*
	* 打开串口
	* @param fd {int } 文件指针
	* @param name {char* } 串口名,如"/dev/ttyS*"
	* @return {int} 返回<1,异常;返回>0,为fd
	*/
	int uart_open(int &fd,const char *name);
	/*
	* 串口配置
	* @param fd {int } 文件指针
	* @param baude {int } 波特率
	* @param c_flow {int } 流标识
	* @param bits {int } 数据位
	* @param parity {char } 奇偶位
	* @param stop {int } 停止位
	* @return {int}  返回=-1,异常;返回=0,成功,为读取大小
	*/
	int uart_config(int fd,int baude,int c_flow, int bits, char parity, int stop);
	/*
	* 串口读取数据
	* @param fd {int } 文件指针
	* @param r_buf {char* } 缓存指针
	* @param lenth {int } 缓存大小
	* @param time_out_ms {等待时间 } 毫秒,默认1000
	* @return {int} 返回<=0,异常;返回>0,成功
	*/
	int uart_read(int fd, char *r_buf, int lenth, int time_out_ms=1000);
	/*
	* 串口写入数据
	* @param fd {int } 文件指针
	* @param r_buf {char* } 内容指针
	* @param lenth {int } 大小
	* @return {int} 返回<=0,异常;返回>0,成功,为写入大小
	*/
	int uart_write(int fd, char *r_buf, int lenth, int time_out_ms=1000);
	/*
	*用于清空输入、输出缓冲区 
	* @param fd {int } 文件指针
	* @param model {int } 模式,有三种取值  TCIFLUSH(用于清空输入缓冲区) TCOFLUSH(用于清空输出缓冲区) TCIOFLUSH(用于清空输入输出缓冲区)
	* @return {int} 
	*/
	int uart_clear(int fd,int model);
	/*
	* 关闭串口
	* @param fd {int } 文件指针
	* @return {int} 
	*/
	int uart_close(int fd);
};

#endif
