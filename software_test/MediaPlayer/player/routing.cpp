#include "routing.h"
#include "player.h"

#include <QMap>

Routing::Routing(Player *_player
                 ,QObject * parent)
    : QThread(parent)
    , player(_player)
{

}

Routing::~Routing()
{

}

void Routing::run()
{
    while (true) {
        Tolcall();
//        this->msleep(1000);
        this->sleep(5);
    }
    exec();
}

void Routing::Tolcall()
{
    QMap<int,int> playStates;
    QMap<int,int>::iterator it;
    player->getState(playStates);
    for(it = playStates.begin();it!=playStates.end();it++){
        emit sendItem(it.key(),it.value());
    }
    playStates.clear();
}
