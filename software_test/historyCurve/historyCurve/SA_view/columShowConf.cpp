#include "columShowConf.h"

#include <QColor>
#include <QPalette>
#include <QFont>
#include <QDebug>
#include <QApplication>
#include <QGridLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QMouseEvent>

ColumShowConf::ColumShowConf(QList<ColumnShow> _list,QWidget * parent,Qt::WindowFlags f)
	: QDialog(parent,f)
{
    win_init();
    data_init(_list);
}

ColumShowConf::~ColumShowConf()
{

}

void ColumShowConf::win_init()
{
    QFont _font("Courier New", 10);
    _font.setBold(true);
    _font.setUnderline(true);
    this->setFont(_font);

    QPalette pl = this->palette();
    pl.setColor(QPalette::Base,QColor(240,240,240));
    pl.setColor(QPalette::Text,QColor(0,0,230));  
    this->setPalette(pl);
    this->setWindowTitle(tr("check colum which is need"));
}

void ColumShowConf::data_init(QList<ColumnShow> _list)
{
    QGridLayout *_QGridLayout = new QGridLayout(this);
    _checklist.clear();
    for (int i=0; i<_list.size(); i++)
    {
        // qDebug() << _list.at(i)._colDesc << "...........ColumShowConf::data_init....\n";
        QCheckBox *_QCheckBox = new QCheckBox(_list.at(i)._colDesc);
        _QCheckBox->setCheckState(_list.at(i)._f?Qt::Checked:Qt::Unchecked);
        _QGridLayout->addWidget(_QCheckBox,i/3,i%3,1,1);
        _checklist.append(_QCheckBox);
    }
    
    okButton = new QPushButton(tr("Ok"));
    cancelButton = new QPushButton(tr("Cancel"));
    connect(okButton,SIGNAL(clicked()),this,SLOT(setHideColum()));
    connect(cancelButton,SIGNAL(clicked()),this,SLOT(close()));
    _QGridLayout->addWidget(okButton,_list.size()/3+1,1,1,1);
    _QGridLayout->addWidget(cancelButton,_list.size()/3+1,2,1,1);
    this->setLayout(_QGridLayout);
}

void ColumShowConf::setHideColum()
{
    QMap<int,bool> _list;
    for (int i = 0; i < _checklist.size(); i++)
    {
        _list[i] = (_checklist.at(i)->checkState()==Qt::Checked)?true:false;
    }
    emit sendColumShow(_list);
    this->close();
}
