#include "cuvre.h"

#include "qwt/qwt_curve_fitter.h"

#include "plotcurve.h"

Cuvre::Cuvre(QString title,QwtPlotCurve::CurveStyle style){
    curve = new MyQwtPlotCurve(/*title*/);
    curve->setSymbol( new QwtSymbol( QwtSymbol::Cross, Qt::NoBrush,
        QPen( Qt::black ), QSize( 5, 5 ) ) );
    curve->setPen( QColor( Qt::darkGreen ) );
//    curve->setBrush(Qt::SolidPattern);
//    curve->setStyle( QwtPlotCurve::Lines );
    curve->setStyle( style );
//    curve->setCurveAttribute( QwtPlotCurve::Fitted );
    curve->setCurveAttribute( QwtPlotCurve::Inverted );
    curve->setSamples( val );
    curve->setCurveInterval(m_CurveInterval);

//    QwtSplineCurveFitter *m_QwtSplineCurveFitter = new QwtSplineCurveFitter();
//    curve->setCurveFitter(m_QwtSplineCurveFitter);
    QwtWeedingCurveFitter *m_QwtWeedingCurveFitter = new QwtWeedingCurveFitter(0.01);
    curve->setCurveFitter(m_QwtWeedingCurveFitter);
    curve->setTitle(title);

//    curve->setPen( s.curve.pen );
//    curve->setBrush( s.curve.brush );

    curve->setPaintAttribute( QwtPlotCurve::ClipPolygons,false );
    curve->setPaintAttribute( QwtPlotCurve::FilterPoints,false );

    curve->setRenderHint( QwtPlotItem::RenderAntialiased,false );
//    curve->setRenderHint( QwtPlotItem::RenderFloats,false );

    /*
    * LegendNoAttribute QwtPlotCurve tries to find a color representing the curve and paints a rectangle with it.
    * LegendShowLine    If the style() is not QwtPlotCurve::NoCurve a line is painted with the curve pen().
    * LegendShowSymbol  If the curve has a valid symbol it is painted.
    * LegendShowBrush   If the curve has a brush a rectangle filled with the curve brush() is painted.
    */
    curve->setLegendAttribute(QwtPlotCurve::LegendShowLine,true);
//    curve->attach(m_QwtPlot);
}

Cuvre::~Cuvre()
{

}

QString Cuvre::getTitle()
{
    return curve->title().text();
}

void Cuvre::attach( QwtPlot *plot )
{
    curve->attach(plot);
}

void Cuvre::detach()
{
    curve->detach();
}

void Cuvre::addPoint(QPointF point)
{
    val.append(point);
}

void Cuvre::addInterval(const int from, const int to)
{
    int newFrom = from;
    int newTo = to;
    if(qwtVerifyRange(curve->data()->size(),newFrom,newTo)<=0)
            return;
    if(!m_CurveInterval.isEmpty()){
        for(int i=0; i<m_CurveInterval.size(); i++){
            if(m_CurveInterval.value(i).getFrom()<newFrom
                    &&m_CurveInterval.value(i).getTo()>newTo){
                //if Symbol and CurveStyle isn't same, neet deal with more
                return;
            }else if(m_CurveInterval.value(i).getFrom()>newFrom
                     &&m_CurveInterval.value(i).getTo()<newTo){
                //if Symbol and CurveStyle isn't same, neet deal with more
                m_CurveInterval.remove(i);
                i--;
            }else if(m_CurveInterval.value(i).getFrom()<newFrom
                     &&m_CurveInterval.value(i).getTo()>newFrom
                     &&m_CurveInterval.value(i).getTo()<newTo){
                newFrom = m_CurveInterval.value(i).getTo();
            }else if(m_CurveInterval.value(i).getFrom()>newFrom
                     &&m_CurveInterval.value(i).getFrom()<newTo
                     &&m_CurveInterval.value(i).getTo()>newTo){
                newTo = m_CurveInterval.value(i).getFrom();
            }
        }
    }

    CurveInterval m_Interval(newFrom,newTo);
    m_CurveInterval.append(m_Interval);
}

void Cuvre::setCurveMove( const double x_Move, const double y_Move)
{
    curve->setCurveMove( x_Move, y_Move);
}

void Cuvre::setBaseline(double baseline)
{
    curve->setBaseline(baseline);
}

void Cuvre::setBrush(QBrush brush)
{
    curve->setBrush(brush);
}

void Cuvre::setSamples()
{
    curve->setSamples( val );
}

void Cuvre::setStyle(QwtPlotCurve::CurveStyle style)
{
    curve->setStyle(style);
}

void Cuvre::setPen(QColor color)
{
    curve->setPen(QPen(color));
}

void Cuvre::setPen(QPen pen)
{
    curve->setPen(pen);
}
void Cuvre::setSymbolStyle(QwtSymbol::Style style)
{
    curve->setSymbol( new QwtSymbol( style, curve->symbol()->brush(),
        curve->symbol()->pen(), curve->symbol()->size() ) );
}

void Cuvre::setSymbolBrush(QBrush brush)
{
    curve->setSymbol( new QwtSymbol( curve->symbol()->style(), brush,
        curve->symbol()->pen(), curve->symbol()->size() ) );
}

void Cuvre::setSymbolPen(QPen pen)
{
    curve->setSymbol( new QwtSymbol( curve->symbol()->style(), curve->symbol()->brush(),
        pen, curve->symbol()->size() ) );
}

void Cuvre::setSymbolSize(QSize size)
{
    curve->setSymbol( new QwtSymbol( curve->symbol()->style(), curve->symbol()->brush(),
        curve->symbol()->pen(), size ) );
}
