#include "histogram.h"

#include "qwt/qwt_curve_fitter.h"

#include "plothistogram.h"

MyHistogram::MyHistogram(QString title,QwtPlotHistogram::HistogramStyle style)
{
    m_QwtPlotHistogram = new MyQwtPlotHistogram(title);
    m_QwtPlotHistogram->setStyle(style);
    m_QwtPlotHistogram->setSamples(val);
    m_QwtPlotHistogram->setMyColorData(&m_Color);
}

MyHistogram::~MyHistogram()
{

}

QString MyHistogram::getTitle()
{
    return m_QwtPlotHistogram->title().text();
}

void MyHistogram::attach( QwtPlot *plot )
{
    m_QwtPlotHistogram->attach(plot);
}

void MyHistogram::detach( )
{
   m_QwtPlotHistogram->detach();
}

void MyHistogram::addData(QwtIntervalSample value, QColor color)
{
    val.append(value);
    m_Color.append(color);
}

void MyHistogram::setSamples()
{
    m_QwtPlotHistogram->setSamples( val );
}