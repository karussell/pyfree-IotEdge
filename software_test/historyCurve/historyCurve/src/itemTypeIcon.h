#ifndef ITEMTYPEICON_HPP
#define ITEMTYPEICON_HPP

#include <QIcon>
#include <QVariant>

enum IconType
{
    Pro_Node = 1,
    CZ_Node = 2,
    Item_Node = 3,

    UnkownType = 99
};

static IconType ToIconType(int level = 0)
{
    IconType val = UnkownType;
    switch(level){
        case 1:
            val = Pro_Node;
            break;
        case 2:
            val = CZ_Node;
            break;
        case 3:
            val = Item_Node;
            break;
        default:
            break;
    }
    return val;
};

static QIcon ToIcon(IconType type)
{
    QIcon val;
    switch(type){
        case 1:
            val = QIcon(":/resource/proconf.png");
            break;
        case 2:
            val = QIcon(":/resource/czconf.png");
            break;
        case 3:
            val = QIcon(":/resource/itemconf.png");
            break;
        default:
            val = QIcon(":/resource/unkown.png");
            break;
    }
    return val;
};

#endif
