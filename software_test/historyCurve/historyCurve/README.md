# pyfree-historycurve

#### 介绍
实现物联网边缘服务系统的调度系统记录数据(sqlite)的历史曲线展示  

#### 软件说明  
* 1)总概述：  
    历史数据曲线展示   
	![启动界面展示示例](/res/for_doc/historycurve_demo.PNG) 

  2)功能描述  
    * 历史数据文件(sqlite)加载     
	* 历史数据列表展示  
	* 历史数据图形化曲线展示  

#### 编译说明  
* 依赖qwt图形库(https://qwt.sourceforge.io/),源码下载，静态编译,本文已提供win64静态编译的qwt库和头文件  
* qt5.8源码安装,并将其静态编译  
* 基于qt5.8的qtcreater编译工具编译qwt源码获得静态库(win64已提供,可以跳过该步)  
* 基于qt5.8的qtcreater编译工具编译本工程  
