#include "testqml.h"

#include "ice_src/PCSClient.h"

MyClass::MyClass()
{
    qDebug() << "ice set!";
#ifdef ICE_STATIC_LIBS
    Ice::registerIceSSL();
#endif
    ptr_PC = new PCSClient();
    qDebug() << "ptr_PC set!";
    connect(ptr_PC, SIGNAL(addDev_signal(QVariant,QVariant,QVariant,QVariant)),
                  this, SIGNAL(addDev(QVariant,QVariant,QVariant,QVariant)));
    connect(ptr_PC, SIGNAL(addPInfo_signal(QVariant,QVariant,QVariant,QVariant,QVariant,QVariant)),
                  this, SIGNAL(addPInfo(QVariant,QVariant,QVariant,QVariant,QVariant,QVariant)));
    connect(ptr_PC, SIGNAL(PValue_signal(QVariant,QVariant,QVariant,QVariant)),
                  this, SIGNAL(PValue(QVariant,QVariant,QVariant,QVariant)));
//    ptr_PC->start();
};

void MyClass::setPValue(int devID,int pID,qreal val)
{
    ptr_PC->setPValue(devID,pID,val);
};

