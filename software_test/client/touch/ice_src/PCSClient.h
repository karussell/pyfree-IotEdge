#ifndef PCS_CLIENT_H
#define PCS_CLIENT_H
#include <QThread>
#include <QVariant>

#include <Ice/Ice.h>
#include <IceUtil/IceUtil.h>
//#include <IceGrid/IceGrid.h>
#include "PCSInterfaceI.h"

using namespace PCS;

class PCSClient : public QThread
{
    Q_OBJECT
public:

    PCSClient(QObject * parent=0);
	~PCSClient();

    void addDev(const ::PCS::Devs &devs);
    void addPInfo(::Ice::Long devID, const ::PCS::PInfos &pinfos);
    void PValueChange(::Ice::Long devID
                              ,::Ice::Long pID
                              , const ::PCS::DateTimeI& itime
                              , ::Ice::Float val);  
protected:
    void run();
private:
    bool connect();
	Ice::CommunicatorPtr communicator();
	void disconnect();
signals:
    void addDev_signal(QVariant devID,QVariant devType,QVariant name, QVariant desc);
    void addPInfo_signal(QVariant devID,QVariant pID,QVariant name,QVariant desc,QVariant pType,QVariant val);
    void PValue_signal(QVariant devID,QVariant pID,QVariant dtime, QVariant val);
public slots:
    void setPValue(int devID,int pID,qreal val);
private:
    int                         runType;
	bool 						m_bConnect;
    bool                        m_activate;
    std::string                 m_strUUID;
	Ice::CommunicatorPtr		m_ic;
    ServerAchievePrx			soneway;
//    ClientAchievePrx            coneway;
};

#endif //PCS_CLIENT_H
