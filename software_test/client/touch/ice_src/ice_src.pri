SOURCES += \
    $$PWD/PCSClient.cpp \
    $$PWD/PCSInterface.cpp \
    $$PWD/PCSInterfaceI.cpp

HEADERS += \
    $$PWD/PCSClient.h \
    $$PWD/PCSInterface.h \
    $$PWD/PCSInterfaceI.h

DEFINES += \
        ICE_STATIC_LIBS \
        ICE_API_EXPORTS \

INCLUDEPATH += $$PWD

win32{
    ICEHOME = C:\\software\\ice-master\\cpp
    ICEC98H = $$ICEHOME/include/generated/cpp98/x64
    ICEC11H = $$ICEHOME/include/generated/cpp11/x64
    ICEBZIP2LIB = $$ICEHOME/msbuild/packages/bzip2.v140.1.0.6.7/build/native/lib/x64
    ICEEXPATLIB = $$ICEHOME/msbuild/packages/expat.v140.2.1.0.5/build/native/lib/x64
    ICEMDBLIB   = $$ICEHOME/msbuild/packages/lmdb.v140.0.9.19.1/build/native/lib/x64
    ICELIB =  $$ICEHOME/lib/x64

    INCLUDEPATH += $$ICEHOME/include
    DEPENDPATH += $$ICELIB
    LIBS       += -L"$$ICELIB"

    LIBS += advapi32.lib \
        pdh.lib \
        ws2_32.lib \
        rpcrt4.lib \
        DbgHelp.lib \
        Shlwapi.lib \
        Iphlpapi.lib \
        crypt32.lib \
        Secur32.lib

    CONFIG(debug, debug|release){
        INCLUDEPATH += $$ICEC98H/Debug \
                $$ICEC11H/Debug

        LIBS += $$ICELIB/Debug/iceutild.lib
        LIBS += $$ICELIB/Debug/ice37d.lib
        LIBS += $$ICELIB/Debug/icessl37d.lib
        LIBS += $$ICELIB/Debug/icediscovery37d.lib
        LIBS += $$ICELIB/Debug/icelocatordiscovery37d.lib
        LIBS += $$ICELIB/Debug/glacier237d.lib
        LIBS += $$ICELIB/Debug/icegrid37d.lib
        #LIBS += $$ICELIB/Debug/icexml37++11d.lib
        LIBS += $$ICEBZIP2LIB/MT-Debug/libbz2d.lib
        LIBS += $$ICEEXPATLIB/Debug/libexpatd.lib
        LIBS += $$ICEMDBLIB/Debug/lmdbd.lib
    }else{
        INCLUDEPATH += $$ICEC98H/Release \
            $$ICEC11H/Release

        LIBS += $$ICELIB/Release/iceutil.lib
        LIBS += $$ICELIB/Release/ice37.lib
        LIBS += $$ICELIB/Release/icessl37.lib
        LIBS += $$ICELIB/Release/icediscovery37.lib
        LIBS += $$ICELIB/Release/icelocatordiscovery37.lib
        LIBS += $$ICELIB/Release/glacier237.lib
        LIBS += $$ICELIB/Release/icegrid37.lib
        #LIBS += $$ICELIB/Release/icexml37++11.lib
        LIBS += $$ICEBZIP2LIB/MT-Release/libbz2.lib
        LIBS += $$ICEEXPATLIB/Release/libexpat.lib
        LIBS += $$ICEMDBLIB/Release/lmdb.lib
    }
    #message($$LIBS)
}
